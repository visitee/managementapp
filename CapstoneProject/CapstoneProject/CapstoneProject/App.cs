﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using CapstoneProject.Pages;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace CapstoneProject
{
    public class App : Application
    {
        //public static string Url = "http://localhost:3000";
        public static string Url = "http://10.20.14.21:3000";
        #region variable
        private VisiteeClient vc;
        public static List<Feature> listfeatures = new List<Feature>();
        public static Dictionary<string, List<Room>> collection;
        public static List<Floor> floors = new List<Floor>();
        public static List<Room> listroom;
        private List<CheckInOut> checkinout;
        private List<CheckInOut> _tempcheckinout;
        private const string image = "CapstoneProject.Resources.img.png";
        private const string color1 = "#FFFFFFFF";
        private const string color2 = "Yellow";
        public static double ScreenWidth;
        #endregion

        public App()
        {
            MainPage = new RootPage();
            //MainPage = new NavigationPage(new FinancialReport());
            Session.OnAuthenticationFinished += OnAuthenticationFinished;
            Session.OnSessionStateChanged += OnSessionStateChanged;
            Session.checkLoginStatus();
            //((RootPage)MainPage).Detail = new NavigationPage(new Main());
            if (String.IsNullOrEmpty(Session.ActiveSession.CurrentAccessTokenData.AccessToken))
            {
                ((RootPage)MainPage).Detail = new NavigationPage(new Login());
            }
        }

        #region LoadData
        private void OnSessionStateChanged(LoginStatus a)
        {
            if (LoginStatus.LoggedIn.Equals(a)) { 
                loadData();
                ((RootPage)MainPage).Master.IsVisible = true;
            }
        }

        private void OnAuthenticationFinished(AccessTokenData session)
        {
            loadData();
            ((RootPage)MainPage).Master.IsVisible = true;
        }

        private async void loadData()
        {
            if (String.IsNullOrEmpty(Session.ActiveSession.CurrentAccessTokenData.AccessToken))
            {
                ((RootPage)MainPage).Detail = new NavigationPage(new Login());
                return;
            }
            vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            floors = await vc.GetAsync<List<Floor>>("floors");
            var listallfeatures = await vc.GetAsync<List<Feature>>("features");
            foreach (var feature in listallfeatures)
            {
                foreach (var permission in Session.ActiveSession.CurrentAccessTokenData.CurrentPermissions)
                {
                    if (permission.feature_id == feature.id)
                    {
                        listfeatures.Add(feature);
                    }
                }
            }

            collection = new Dictionary<string, List<Room>>();
            listroom = new List<Room>();
            checkinout = new List<CheckInOut>();
            _tempcheckinout = new List<CheckInOut>();
            await Task.Factory.StartNew(_datalistener);
            ((RootPage)MainPage).Detail = new NavigationPage(new Main());
        }


        private async void _datalistener()
        {
            while (true)
            {

                try
                {
                    listroom = await vc.GetAsync<List<Room>>("rooms");
                    checkinout = await vc.GetAsync<List<CheckInOut>>("checkinouts");
                }
                catch
                {
                    vc.cancelPendingRequests();
                }


                foreach (Room r in listroom)
                {
                    r.image = image;
                    if (r.status == 1)
                        r.color = color1;
                    else {
                        r.color = color2;
                        for (int i = 0; i < checkinout.Count; i++)
                        {
                            if (checkinout[i].room_id == r.id)
                            {
                                r.lastcheckin = checkinout[i].created_at;
                                
                            }
                        }
                    }
                }

                bool con = false;

                try
                {
                    var x = (Main)((NavigationPage)((RootPage)MainPage).Detail).CurrentPage;
                    con = true;
                }
                catch(Exception ex)
                {

                }

                if (con)
                {
                    if (_tempcheckinout.Count < checkinout.Count)
                    // if(true)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ((Main)((NavigationPage)((RootPage)MainPage).Detail).CurrentPage).setBindingListview();
                        });

                    }

                }
                _tempcheckinout = checkinout;
                await Task.Delay(1000);
            }
        }
        
        #endregion



        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

    }

}
