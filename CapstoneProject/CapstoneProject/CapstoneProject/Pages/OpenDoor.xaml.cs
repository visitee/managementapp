﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Sockets.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CapstoneProject.Pages
{
    public partial class OpenDoor : ContentPage
    {
        public OpenDoor()
        {
            InitializeComponent();
            this.Appearing += OpenDoor_Appearing;
        }
        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        Room currentroom = new Room();
        List<Room> listroom = new List<Room>();

        private async void OpenDoor_Appearing(object sender, EventArgs e)
        {
            try
            {
                listroom = await vc.GetAsync<List<Room>>("rooms");
                foreach (var room in listroom)
                {
                    RoomsList.Items.Add(room.name);
                }
                /*foreach(var card in rfid)
                {
                    RfidCardsList.Items.Add(card.name);
                }*/
                AnimatedButton ab = new AnimatedButton("Mở cửa", "CapstoneProject.Resources.Open.png", StackOrientation.Horizontal, () =>
                {
                    OnOpenClicked();
                });
                layout.Children.Add(ab);
            }
            catch
            {
            }
        }

        private void OnRoomSelectionChanged(object sender, EventArgs e)
        {
            var y = ((Picker)sender).Items[((Picker)sender).SelectedIndex];
            foreach (var room in listroom)
            {
                if (room.name.Equals(y))
                {
                    currentroom = room;

                    break;
                }
            }
            
        }

        private async void OnOpenClicked()
        {
            string ipaddress = "";
            try
            {
                var listdevice = await vc.GetAsync<List<ArduinoDevice>>("devices");
                foreach (var device in listdevice)
                {
                    if (device.id.Equals(currentroom.device_id))
                        ipaddress = device.ipaddress;
                }
                TcpSocketClient client = new TcpSocketClient();
                await client.ConnectAsync(ipaddress, 23);
                foreach (char a in (currentroom.no + "*").ToCharArray())
                    client.WriteStream.WriteByte(Convert.ToByte(a));
                await client.WriteStream.FlushAsync();
                await client.DisconnectAsync();
            }

            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }
    }
}
