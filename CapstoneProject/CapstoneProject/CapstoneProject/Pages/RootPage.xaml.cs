﻿using CapstoneProject.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CapstoneProject.Pages
{
    public partial class RootPage : MasterDetailPage
    {
        public RootPage()
        {
            InitializeComponent();
            Detail = new NavigationPage(new OpenDoor());
            masterPage.MenuList.ItemSelected += MenuList_ItemSelected;
            Master.IsVisible = false;
        }
        
        private void MenuList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var menu = e.SelectedItem as MenuItem;
            if (menu == null)
                return;
            if(menu.TargetType.Equals(typeof(Login)))
            {
                Session.ActiveSession.Logout();
                Detail = new NavigationPage(new Login());
                Master.IsVisible = false;
                return;
            }
            Page displayPage = (Page)Activator.CreateInstance(menu.TargetType);

            Detail = new NavigationPage(displayPage);

            masterPage.MenuList.SelectedItem = null;
            IsPresented = false;
        }
    }
}
