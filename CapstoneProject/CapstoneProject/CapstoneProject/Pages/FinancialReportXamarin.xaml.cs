﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Syncfusion.SfChart.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CapstoneProject
{
    public partial class FinancialReportXamarin : ContentPage
    {
        public FinancialReportXamarin()
        {
            InitializeComponent();
            (Chart.SecondaryAxis as NumericalAxis).Maximum = 9000000;
            (Chart.SecondaryAxis as NumericalAxis).Minimum = 0;
            (Chart.SecondaryAxis as NumericalAxis).Interval = 100000;

            ((Chart.Series[1] as FastLineSeries).YAxis as NumericalAxis).Maximum = 600;
            ((Chart.Series[1] as FastLineSeries).YAxis as NumericalAxis).Minimum = 0;
            ((Chart.Series[1] as FastLineSeries).YAxis as NumericalAxis).Interval = 20;
            ((Chart.Series[1] as FastLineSeries).YAxis as NumericalAxis).OpposedPosition = true;
            this.Appearing += FinancialReport_Appearing;
        }

        private async void FinancialReport_Appearing(object sender, EventArgs e)
        {
            NavigationPage.SetHasNavigationBar(this, false);
            ViewModel vm = new ViewModel();
           // columnseries.ItemsSource = vm.CircularData;
           // lineseries.ItemsSource = vm.datas1;

            AnalysisViewModel avm = new AnalysisViewModel();
            await avm.SetData(AnalyzeType.MonthInYear);
            columnseries.ItemsSource = avm.SalesData;
            (Chart.SecondaryAxis as NumericalAxis).Maximum = avm.max;
            lineseries.ItemsSource = avm.SalesData;
        }
    }


    public enum AnalyzeType
    {
        DayInMonth, MonthInYear, QuaterInYear, Year
    }

    public class AnalysisViewModel
    {
        public ObservableCollection<AnalysisModel> SalesData { get; set; }

        public AnalysisViewModel()
        {
        }

        public double max = 0;
        public async Task SetData(AnalyzeType type)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            List<Billing> billinglist = await vc.GetSqlAsync<List<Billing>>("select * from billings where status = 1 order by created_at");
            ObservableCollection<AnalysisModel> datas = new ObservableCollection<AnalysisModel>();
            string current = "";
            foreach (var bill in billinglist)
            {
                var value = bill.valuevnd;
                if (type.Equals(AnalyzeType.DayInMonth))
                {
                    if (current.Equals(bill.created_at.Date.ToString()))
                    {
                        if (!current.Equals(""))
                        {
                            datas.Last<AnalysisModel>().NoOfCustomer++;
                            datas.Last<AnalysisModel>().Income += value;
                        }
                    }
                    else
                    {
                        datas.Add(new AnalysisModel(bill.created_at.Date.ToString(), 1, value, ""));
                        current = bill.created_at.Date.ToString();
                    }
                }
                else if (type.Equals(AnalyzeType.MonthInYear))
                {
                    if (current.Equals(bill.created_at.Month.ToString()))
                    {
                        datas.Last<AnalysisModel>().NoOfCustomer++;
                        datas.Last<AnalysisModel>().Income += value;
                        if (max < datas.Last<AnalysisModel>().Income)
                            max = datas.Last<AnalysisModel>().Income;
                    }
                    else
                    {
                        datas.Add(new AnalysisModel(bill.created_at.Month.ToString(), 1, value, ""));
                        current = bill.created_at.Month.ToString();
                    }
                }
                else if (type.Equals(AnalyzeType.QuaterInYear))
                {
                    if (current.Equals(bill.created_at.Month.ToString()))
                    {
                        datas.Last<AnalysisModel>().NoOfCustomer++;
                        datas.Last<AnalysisModel>().Income += value;
                    }
                    else
                    {
                        datas.Add(new AnalysisModel(bill.created_at.Month.ToString(), 1, value, ""));
                        current = bill.created_at.Month.ToString();
                    }
                }

            }
            SalesData = datas;
        }
    }

    public class AnalysisModel
    {

        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        private double income;

        public double Income
        {
            get { return income; }
            set { income = value; }
        }

        private double noOfCustomer;

        public double NoOfCustomer
        {
            get { return noOfCustomer; }
            set { noOfCustomer = value; }
        }

        private string time;
        

        public string Time
        {
            get { return time; }
            set { time = value; }
        }

        public AnalysisModel(string year, double count, double income, string text)
        {
            Time = year;
            NoOfCustomer = count;
            Income = income;
            Text = text;
        }
    }
}
