﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CapstoneProject
{
    public partial class CheckInLogReportXamarin : ContentPage
    {
        public CheckInLogReportXamarin()
        {
            InitializeComponent();
            //axis2.Minimum = DateTime.Now.AddDays(-5);
            axis2.Maximum = DateTime.Now;
            //axis2.MaximumLabels = 12;
            this.Appearing += CheckInLogReportXamarin_Appearing;
        }

        private ObservableCollection<ModelCheckInLog> scatterseriesmodel;
        private ObservableCollection<ModelCheckInLog> hiloseriesmodel;

        private async void CheckInLogReportXamarin_Appearing(object sender, EventArgs e)
        {
            scatterseriesmodel = new ObservableCollection<ModelCheckInLog>();
            hiloseriesmodel = new ObservableCollection<ModelCheckInLog>();

            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            List<Room> listroom = await vc.GetAsync<List<Room>>("rooms");
            foreach (var room in listroom)
            {
                //listroomdic.Add(room.id, room);
                var a = await vc.GetSqlAsync<List<CheckInOut>>("select * from checkinouts where room_id=" + room.id);
                var b = await vc.GetSqlAsync<List<Booking>>("select * from bookings where room_id=" + room.id);
                foreach (var checkinout in a)
                {
                    scatterseriesmodel.Add(new ModelCheckInLog() { XAxis = room.id, YAxis = 42444.040953425923  
                    });
                }

                foreach (var booking in b)
                {
                    hiloseriesmodel.Add(new ModelCheckInLog() { CheckIn = booking.created_at.ToFileTimeUtc() / 86400 + 25569, CheckOut = booking.updated_at.ToFileTimeUtc() / 86400 + 25569, XAxis = room.id, tex = room.name });
                }
            }

            scatterseries.ItemsSource = scatterseriesmodel;
            rangeseries.ItemsSource = hiloseriesmodel;
        }
    }

    public class ModelCheckInLog
    {
        public double CheckIn { get; set; }
        public double CheckOut { get; set; }
        public double XAxis { get; set; }
        public double YAxis { get; set; }
        public string LabelContent { get; set; }
        public string tex { get; set; }
    }

}
