﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CapstoneProject
{
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();
            //CreateLeftMenu();
        }
      
        /// <summary>
        /// Creates the right side menu panel
        /// </summary>
        public void CreateLeftMenu()
        {

            MenuLayout.Children.Clear();
            MenuLayout.Padding = 15;
            MenuLayout.VerticalOptions = LayoutOptions.FillAndExpand;
            MenuLayout.HorizontalOptions = LayoutOptions.Start;
            MenuLayout.BackgroundColor = Color.FromRgba(0, 0, 0, 180);

                foreach (var item in App.listfeatures)
                {
                    AnimatedButton ab = new AnimatedButton(item.namevn, () =>
                    {
                    });
                MenuLayout.Children.Add(ab);
                }
            }
        }
        



        /// <summary>
        /// Animates the panel in our out depending on the state
        /// </summary>
}
