﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Syncfusion.SfDataGrid.XForms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CapstoneProject.Pages
{
    public partial class ListTable : ContentPage
    {
        private Dictionary<Type, ObservableCollection<MappingName>> tableColumnMapping = new Dictionary<Type, ObservableCollection<MappingName>>();
        private ObservableCollection<ListMappingName> listTable = new ObservableCollection<ListMappingName>();
        private Type currentType;

        public ListTable()
        {
            InitializeComponent();
            viewModel.filtertextchanged = OnFilterChanged;
            initTableColumn();
            initListTableCombobox();
            addListComboItem();
        }

        void OnFilterChanged()
        {
            if (dataGrid.View != null)
            {
                this.dataGrid.View.Filter = viewModel.FilerRecords;
                this.dataGrid.View.RefreshFilter();
            }
        }

        void OnFilterTextChanged(object sender, TextChangedEventArgs e)
        {
            if (e.NewTextValue == null)
                viewModel.FilterText = "";
            else
                viewModel.FilterText = e.NewTextValue;
        }

        private void addListComboItem()
        {
            foreach (var item in listTable)
            {
                ColumnsList.Items.Add(item.Header);
            }
        }

        private void addColumnComboItem(Type o)
        {
            var list = tableColumnMapping[o];
            dataGrid.Columns.Clear();
              foreach (var mapping in list)
            {
                GridTextColumn column = new GridTextColumn();
                column.HeaderText = mapping.Header;
                column.MappingName = mapping.Name;
                dataGrid.Columns.Add(column);
            }

        }


        async void OnColumnsSelectionChanged(object sender, EventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            var x = e;
            var y = sender;
            Picker picker = (Picker)sender;
            string z = picker.Items[picker.SelectedIndex];
            try { 
            
                object t = new object();
                if (z.Equals(Properties.Resources.ListTable_Employee))
                {
                    addColumnComboItem(typeof(Employee));
                    t = await vc.GetAsync<List<Employee>>("employees");
                }
                else if (z.Equals(Properties.Resources.ListTable_Department))
                {
                    addColumnComboItem(typeof(Department));
                    t = await vc.GetAsync<List<Department>>("departments");
                }
                else if (z.Equals(Properties.Resources.ListTable_Booking))
                {
                    addColumnComboItem(typeof(Booking));
                    t = await vc.GetAsync<List<Booking>>("bookings");
                }
                else if (z.Equals(Properties.Resources.ListTable_Guest))
                {
                    addColumnComboItem(typeof(Guest));
                    t = await vc.GetAsync<List<Guest>>("guests");
                }
                else if (z.Equals(Properties.Resources.ListTable_Service))
                {
                    addColumnComboItem(typeof(Service));
                    t = await vc.GetAsync<List<Service>>("services");
                }
                else if (z.Equals(Properties.Resources.ListTable_Room))
                {
                    addColumnComboItem(typeof(Room));
                    t = await vc.GetAsync<List<Room>>("rooms");
                }
                else if (z.Equals(Properties.Resources.ListTable_Rfid))
                {
                    addColumnComboItem(typeof(RfidCard));
                    t = await vc.GetAsync<List<RfidCard>>("rfidcards");
                    var listroom = await vc.GetAsync<List<Room>>("rooms");
                    List<RfidCard> list = (List<RfidCard>)t;
                    foreach (var card in list)
                    {
                        foreach (var room in listroom)
                        {
                            if (room.id.Equals(card.room_id))
                                card.room = room.name;
                        }
                    }
                }
                else if (z.Equals(Properties.Resources.ListTable_Billing))
                {
                    addColumnComboItem(typeof(Billing));
                    t = await vc.GetAsync<List<Billing>>("billings");
                }
                else if (z.Equals(Properties.Resources.ListTable_Serviceorder))
                {
                    addColumnComboItem(typeof(ServiceOrder));
                    t = await vc.GetAsync<List<ServiceOrder>>("serviceorders");
                }
                else if (z.Equals(Properties.Resources.ListTable_Floor))
                {
                    addColumnComboItem(typeof(Floor));
                    t = await vc.GetAsync<List<Floor>>("floors");
                }
                else if (z.Equals(Properties.Resources.ListTable_Job))
                {
                    addColumnComboItem(typeof(Job));
                    t = await vc.GetAsync<List<Job>>("jobs");
                }
                dataGrid.ItemsSource = t;
            }
            catch
            {

            }
        }
        private void initTableColumn()
        {
            #region xong cai khoi tao cac cot trong bang

            ObservableCollection<MappingName> serviceOrdercollection = new ObservableCollection<MappingName>();
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_ServiceOrder_serviceid, Name = "service_id" });
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_ServiceOrder_quantity, Name = "quantity" });
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_ServiceOrder_description, Name = "description" });
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_ServiceOrder_guestid, Name = "guest_id" });
            tableColumnMapping.Add(typeof(ServiceOrder), serviceOrdercollection);

            ObservableCollection<MappingName> floorcollection = new ObservableCollection<MappingName>();
            floorcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            floorcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Floor_name, Name = "name" });
            floorcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Floor_description, Name = "description" });
            tableColumnMapping.Add(typeof(Floor), floorcollection);

            ObservableCollection<MappingName> jobcollection = new ObservableCollection<MappingName>();
            jobcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            jobcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Job_title, Name = "title" });
            jobcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Job_level, Name = "level" });
            jobcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Job_description, Name = "description" });
            tableColumnMapping.Add(typeof(Job), jobcollection);


            ObservableCollection<MappingName> billingcollection = new ObservableCollection<MappingName>();
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_valuevnd, Name = "valuevnd" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_valueusd, Name = "valueusd" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_status, Name = "status" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_overtime, Name = "overtime" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_overvaluevnd, Name = "overvaluevnd" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_overvalueusd, Name = "overvalueusd" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_guestid, Name = "guest_id" });
            tableColumnMapping.Add(typeof(Billing), billingcollection);


            ObservableCollection<MappingName> roomcollection = new ObservableCollection<MappingName>();
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_name, Name = "name" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_no, Name = "no" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_status, Name = "status" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_desc, Name = "description" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_floor, Name = "floor_id" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_type, Name = "roomtype_id" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_deviceId, Name = "device_id" });
            tableColumnMapping.Add(typeof(Room), roomcollection);

            ObservableCollection<MappingName> guestcollection = new ObservableCollection<MappingName>();
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_name, Name = "name" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_desc, Name = "description" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_phone, Name = "phone" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_guestType, Name = "guesttype" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_count, Name = "count" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_csID, Name = "cstidno" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_national, Name = "nationality" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_userID, Name = "user_id" });
            tableColumnMapping.Add(typeof(Guest), guestcollection);

            ObservableCollection<MappingName> departmentcollection = new ObservableCollection<MappingName>();
            departmentcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            departmentcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Department_name, Name = "name" });
            departmentcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Department_desc, Name = "description" });
            departmentcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Department_totalE, Name = "totalemployees" });
            tableColumnMapping.Add(typeof(Department), departmentcollection);


            ObservableCollection<MappingName> employeecollection = new ObservableCollection<MappingName>();
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Name, Name = "name" });
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Gender, Name = "gender" });
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Phone, Name = "phone" });
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Description, Name = "description" });
            tableColumnMapping.Add(typeof(Employee), employeecollection);

            ObservableCollection<MappingName> servicecollection = new ObservableCollection<MappingName>();
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Name, Name = "name" });
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Service_Code, Name = "code" });
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Service_Price, Name = "price" });
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Description, Name = "description" });
            tableColumnMapping.Add(typeof(Service), servicecollection);

            ObservableCollection<MappingName> rfidcollection = new ObservableCollection<MappingName>();
            rfidcollection.Add(new MappingName { Header = Properties.Resources.MappingName_Id, Name = "id" });
            rfidcollection.Add(new MappingName { Header = Properties.Resources.MappingName_Employee_Name, Name = "name" });
            rfidcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Description, Name = "description" });
            rfidcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Rfid_Room, Name = "room" });
            tableColumnMapping.Add(typeof(RfidCard), rfidcollection);

            // Hello VN


            ObservableCollection<MappingName> bookingcollection = new ObservableCollection<MappingName>();
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_roomID, Name = "room_id" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_guestID, Name = "guest_id" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_status, Name = "status" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_createdAt, Name = "created_at" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_updatedAt, Name = "updated_at" });
            tableColumnMapping.Add(typeof(Booking), bookingcollection);

            #endregion xong
            //emp, ser, rf, boki, depar, guest, rôm, billing

        }

        private void initListTableCombobox()
        {
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Employee, type = typeof(Employee) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Department, type = typeof(Department) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Booking, type = typeof(Booking) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Guest, type = typeof(Guest) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Service, type = typeof(Service) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Room, type = typeof(Room) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Rfid, type = typeof(RfidCard) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Billing, type = typeof(Billing) });
            //  listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Feature, type = typeof(Feature) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Serviceorder, type = typeof(ServiceOrder) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Floor, type = typeof(Floor) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Department, type = typeof(Department) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Job, type = typeof(Job) });

            //cần thêm danh mục user/usergroup/roomtype/device/ lưu ý là không có feature
            /*  Floor, Department, Job  */
        }
    }

    public class MappingName
    {
        public string Header { get; set; }
        public string Name { get; set; }
    }

    public class ListMappingName
    {
        public string Header { get; set; }
        public Type type { get; set; }
    }
}
