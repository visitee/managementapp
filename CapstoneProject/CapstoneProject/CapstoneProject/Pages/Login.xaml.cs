﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CapstoneProject.Pages
{
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            
        }


        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private async void OnLoginClicked(object sender, EventArgs args)
        {
            try { 
            //username.Text = "admin1";
            //password.Text = "password1";
            string pass = password.Text;
            string usern = username.Text;
            //((Button)sender).Text = Base64Encode(usern + ":" + pass);
            // root.Navigation.PushAsync(new MainPage());
            // root.Navigation.PushModalAsync(new MainPage());
            // 
            //  RunAsync();
            await ButtonLogin.ScaleTo(0.95, 50, Easing.CubicOut);
            await ButtonLogin.ScaleTo(1, 50, Easing.CubicIn);
            //await Navigation.PushAsync(new MainPage());
            await Session.ActiveSession.LoginAsync("abc", true, Base64Encode(usern + ":" + pass));
     
            }
            catch
            {
               await DisplayAlert("Login fail",Properties.Resources.Login_Fail_Message, "OK");
            }
        }
    }
}
