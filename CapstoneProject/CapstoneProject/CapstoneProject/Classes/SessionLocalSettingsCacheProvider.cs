﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using SQLite;
using PCLStorage;
using CapstoneProject.Model;
using Newtonsoft.Json;

namespace CapstoneProject.Classes
{
    public class SessionLocalSettingsCacheProvider : AccessTokenDataCacheProvider
    {
        public override void DeleteSessionData()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            SQLiteConnection connection = new SQLiteConnection(rootFolder.Path + "\\development.sqlite3");

            connection.DeleteAll<Token>();
            connection.Commit();
            connection.Close();
        }

        public override AccessTokenData GetSessionData()
        {
           
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFolder roamingFolder = FileSystem.Current.RoamingStorage;

            //var connection = new SQLiteConnection(System.IO.Path.Combine(rootFolder.Path, "development.sqlite3"));
            //return null;
            SQLiteConnection connection = new SQLiteConnection(rootFolder.Path + "\\development.sqlite3");
           int x = connection.CreateTable<Token>();

            TableQuery<Token> result = connection.Table<Token>();
           
            if (result.Count() == 0)
            {
                connection.Close();
                return null;
            }

            var first = result.ElementAt<Token>(0);
            var session = new AccessTokenData
            {
                AccessToken = first.token,
                Expires = first.expries,
                userId = first.userid,
                userGroupId = first.usergroupid,
                CurrentPermissions = JsonConvert.DeserializeObject<List<Permission>>(first.jsonpermission)
            };
            //connection.CreateTable<User>();
            connection.Commit();
            connection.Close();
            return session;
        }

        public override void SaveSessionData(AccessTokenData data)
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            //var connection = new SQLiteConnection(System.IO.Path.Combine(rootFolder.Path, "development.sqlite3"), SQLiteOpenFlags.ReadWrite);
            //var connection = new SQLiteConnection(System.IO.Path.Combine(rootFolder.Path, "development.sqlite3"));

            SQLiteConnection connection = new SQLiteConnection(rootFolder.Path + "\\development.sqlite3");

            int status = connection.CreateTable<Token>();

            if (connection.Table<Token>().Count() > 0)
                connection.DeleteAll<Token>();
            
            Token x = new Token();
            x.token = data.AccessToken;
            x.expries = data.Expires.AddDays(1);
            x.userid = data.userId;
            x.usergroupid = data.userGroupId;
            x.jsonpermission = JsonConvert.SerializeObject(data.CurrentPermissions);
            connection.Insert(x);

            TableQuery<Token> result = connection.Table<Token>();
            var a = result.Count();
            connection.Commit();
            connection.Close();
        }
    }
}
