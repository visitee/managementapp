﻿using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Classes
{
    public class AccessTokenData
    {
        public AccessTokenData()
        {
            this.CurrentPermissions = new List<Permission>();
        }
        
        public int userId { get; set; }
        public string AccessToken { get; set; }
        public DateTime Expires { get; set; }
        public List<Permission> CurrentPermissions { get; set; }
        public int userGroupId { get; set; }
        public string State { get; set; }
    }
}
