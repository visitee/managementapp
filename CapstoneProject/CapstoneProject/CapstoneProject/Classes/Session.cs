﻿using CapstoneProject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Classes
{

    public enum LoginStatus
    {
        LoggedIn,
        LoggedOut
    }

    public delegate void AuthenticationDelegate(AccessTokenData session);

    public delegate void SessionStateChanged(LoginStatus status);


    public class Session
    {
        public Session()
        {

        }

        public Session(int userId)
        {
            // Also save it as part of the static session object
            CurrentAccessTokenData.userId = userId;
        }

        private AccessTokenData _currentAccessTokenData = null;
        private bool firstRun = true;
        public bool LoginInProgress { get; set; }

        public AccessTokenData CurrentAccessTokenData
        {
            get
            {
                if (firstRun)
                {
                    AccessTokenData tmpSession = AccessTokenDataCacheProvider.Current.GetSessionData();
                    if (tmpSession == null)
                    {
                        _currentAccessTokenData = new AccessTokenData();
                        _currentAccessTokenData.AccessToken = String.Empty;
                        _currentAccessTokenData.userId = 0;
                    }
                    else
                    {
                        _currentAccessTokenData = tmpSession;
                    }

                    firstRun = false;
                }

                if (_currentAccessTokenData != null)
                {
                    if (String.IsNullOrEmpty(_currentAccessTokenData.AccessToken))
                        //&& _currentAccessTokenData.Expires < DateTime.UtcNow)
                    {
                        _currentAccessTokenData.AccessToken = String.Empty;
                        _currentAccessTokenData.CurrentPermissions = new List<Permission>();
                        _currentAccessTokenData.Expires = DateTime.MinValue;
                        _currentAccessTokenData.userId = 0;
                    }
                }

                return _currentAccessTokenData;
            }

            set
            {
                _currentAccessTokenData = value;
                if (value == null)
                {
                    AccessTokenDataCacheProvider.Current.DeleteSessionData();
                    if (Session.OnSessionStateChanged != null)
                    {
                        OnSessionStateChanged(LoginStatus.LoggedOut);
                    }

                    return;
                }

                AccessTokenDataCacheProvider.Current.SaveSessionData(_currentAccessTokenData);
            }
        }

        public static Session ActiveSession = new Session();

        public static AuthenticationDelegate OnAuthenticationFinished;

        public static SessionStateChanged OnSessionStateChanged;

        public async Task<AccessTokenData> LoginAsync(string permissions, bool force, string authorization)
        {
            if (this.LoginInProgress)
            {
                //throw new InvalidOperationException("Login in progress.");
            }
            this.LoginInProgress = true;
            try
            {
                var session = AccessTokenDataCacheProvider.Current.GetSessionData();
                session = null;
                if (session == null)
                {
                    // Authenticate
                    VisiteeClient vc = new VisiteeClient(authorization, VisiteeClient.VisiteeClientType.BasicAuthorization);
                    AccessTokenData accesstokendata = await vc.GetAsync<AccessTokenData>("token");
                    session = accesstokendata;
                }
                else
                {
                    bool newPermissions = false;
                }


                // Save session data
                AccessTokenDataCacheProvider.Current.SaveSessionData(session);
                CurrentAccessTokenData = session;
                OnAuthenticationFinished(CurrentAccessTokenData);

            }
            catch { }
            return CurrentAccessTokenData;
        }

        public void Logout()
        {
            AccessTokenDataCacheProvider.Current.DeleteSessionData();
            ActiveSession.CurrentAccessTokenData = null;
            OnSessionStateChanged(LoginStatus.LoggedOut);
        }

        public static void checkLoginStatus()
        {
            try
            {
                var session = AccessTokenDataCacheProvider.Current.GetSessionData();
                
                if (session == null)
                {
                    OnSessionStateChanged(LoginStatus.LoggedOut);
                    return;
                }
                else
                {
                    VisiteeClient vc = new VisiteeClient(session.AccessToken, VisiteeClient.VisiteeClientType.Token);
                    var users = vc.GetAsync<User>("users");
                    OnSessionStateChanged(LoginStatus.LoggedIn);

                }
                
            }
            catch
            {
                OnSessionStateChanged(LoginStatus.LoggedOut);
            }
        }

        public static Dictionary<string, object> Parameters = new Dictionary<string, object>();
    }
}
