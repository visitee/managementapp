﻿using CapstoneProject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CapstoneProject.Classes
{
    public class VisiteeClient
    { 
        public enum VisiteeClientType
        {
            Token,
            BasicAuthorization
        };
        
        private HttpClient client; 
        public VisiteeClient(string input, VisiteeClientType type)
        {
            InitJsonPairPara();
            InitFeatureImage();
            client = new HttpClient();
            client.BaseAddress = new Uri(App.Url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            if (type.Equals(VisiteeClientType.Token))
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", input);
            else if (type.Equals(VisiteeClientType.BasicAuthorization))
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", input);
        }


        // GET
        public async Task<T> GetAsync<T>(string parameter)
        {
            HttpResponseMessage response = await client.GetAsync(parameter);
            var a = response.Content.ReadAsStringAsync();
            var y = JsonConvert.DeserializeObject<T>(a.Result);
            return y;
        }

        // POST
        public async Task<string> PostAsync<T>(T value)
        {
            StringContent stringContent = new StringContent(
                 "{\"" + GetJsonPairPara<T>().JsonKey + "\":" + JsonConvert.SerializeObject(value) + "}",
                 Encoding.UTF8,
                 "application/json");
            HttpResponseMessage response = await client.PostAsync(GetJsonPairPara<T>().Path, stringContent);
            return await response.Content.ReadAsStringAsync();
        }

        //PUT
        public async void UpdateAsync<T>(T value, int id)
        {
            StringContent stringContent = new StringContent(
                "{\"" + GetJsonPairPara<T>().JsonKey + "\":" + JsonConvert.SerializeObject(value) + "}",
                Encoding.UTF8,
                "application/json");
            await client.PutAsync(GetJsonPairPara<T>().Path + "/" + id, stringContent);
        }

        //DELETE
        public async Task DeleteAsync<T>(int id)
        {
            await client.DeleteAsync(GetJsonPairPara<T>().Path + "/" + id);
        }

        //GET-SQL
        public async Task<T> GetSqlAsync<T>(string statement)
        {

            StringContent stringContent = new StringContent(
                 "{\"sql\":{ \"statement\": \"" + statement + "\"}}",
                 Encoding.UTF8,
                 "application/json");
            HttpResponseMessage response = await client.PostAsync(GetJsonPairPara<T>().Path, stringContent);
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }

        public void cancelPendingRequests()
        {
            client.CancelPendingRequests();
        }

        private Dictionary<Type, JsonPair> JsonPostDictionary = new Dictionary<Type, JsonPair>();
        private Dictionary<int, string> FeatureImages = new Dictionary<int, string>();
        public string getFeatureImage(int idfeature)
        {
            return FeatureImages[idfeature];
        }

        private void InitJsonPairPara()
        {
            JsonPostDictionary.Add(typeof(UserCreate), new JsonPair("user", "users")); 
            JsonPostDictionary.Add(typeof(Usergroup), new JsonPair("usergroup", "usergroups"));
            JsonPostDictionary.Add(typeof(UsergroupCreate), new JsonPair("usergroup", "usergroups"));
            JsonPostDictionary.Add(typeof(EmployeeCreate), new JsonPair("employee", "employees"));
            JsonPostDictionary.Add(typeof(GuestCreate), new JsonPair("guest", "guests"));
            JsonPostDictionary.Add(typeof(BookingCreate), new JsonPair("booking", "bookings"));
            JsonPostDictionary.Add(typeof(RoomCreate), new JsonPair("room", "rooms"));
            JsonPostDictionary.Add(typeof(FeatureCreate), new JsonPair("feature", "features"));
            JsonPostDictionary.Add(typeof(Employee), new JsonPair("employee", "employees"));
            JsonPostDictionary.Add(typeof(Billing), new JsonPair("billing", "billings"));
            JsonPostDictionary.Add(typeof(List<Billing>), new JsonPair("billing", "billings"));
            JsonPostDictionary.Add(typeof(List<CheckInOut>), new JsonPair("checkinout", "checkinouts"));
            JsonPostDictionary.Add(typeof(BillCreate), new JsonPair("billing", "billings"));
            JsonPostDictionary.Add(typeof(ServiceNew), new JsonPair("service", "services"));
            JsonPostDictionary.Add(typeof(GuestdetailCreate), new JsonPair("guestdetail", "guestdetails"));
            JsonPostDictionary.Add(typeof(ServiceOrderCreate), new JsonPair("serviceorder", "serviceorders"));
            JsonPostDictionary.Add(typeof(PermissionCreate), new JsonPair("permission", "permissions"));
            JsonPostDictionary.Add(typeof(Permission), new JsonPair("permission", "permissions")); 
            JsonPostDictionary.Add(typeof(Room), new JsonPair("room", "rooms"));
            JsonPostDictionary.Add(typeof(List<Room>), new JsonPair("room", "rooms"));
            JsonPostDictionary.Add(typeof(List<ServiceOrder>), new JsonPair("serviceorder", "serviceorders"));
            JsonPostDictionary.Add(typeof(List<Booking>), new JsonPair("booking", "bookings"));
            JsonPostDictionary.Add(typeof(List<Feature>), new JsonPair("feature", "features")); 
            JsonPostDictionary.Add(typeof(List<Guest>), new JsonPair("guest", "guests"));
            JsonPostDictionary.Add(typeof(Booking), new JsonPair("booking", "bookings")); 
            JsonPostDictionary.Add(typeof(User), new JsonPair("user", "users"));
            JsonPostDictionary.Add(typeof(Service), new JsonPair("service", "services"));
            JsonPostDictionary.Add(typeof(Guest), new JsonPair("guest", "guests"));
        }

        private void InitFeatureImage()
        { 
            FeatureImages.Add(1, Properties.Resources.Svg_Room);
            FeatureImages.Add(2, Properties.Resources.Svg_CheckIn);
            FeatureImages.Add(3, Properties.Resources.svg_CheckOut);
            FeatureImages.Add(4, Properties.Resources.Svg_calculator);
            FeatureImages.Add(5, Properties.Resources.Svg_setting);
            FeatureImages.Add(6, Properties.Resources.svg_barchard);
            FeatureImages.Add(7, Properties.Resources.Svg_about_excerpt); 
            FeatureImages.Add(8, Properties.Resources.Svg_book);
            FeatureImages.Add(9, Properties.Resources.Svg_bell); 

        }

        public JsonPair GetJsonPairPara<T>()
        {
            return JsonPostDictionary[typeof(T)];
        }

    }

    public class JsonPair
    {
        public JsonPair(string jsonkey, string path)
        {
            this.jsonkey = jsonkey;
            this.path = path;
        }
        public string JsonKey { get { return jsonkey; } set { this.jsonkey = JsonKey; } }
        public string Path { get { return path; } set { this.jsonkey = JsonKey; } }
        private string jsonkey;
        private string path;
    }
}
