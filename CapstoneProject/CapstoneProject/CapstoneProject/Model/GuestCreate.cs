﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class GuestCreate
    {
        public GuestCreate(string name,string phone, int guesttype, int count, string cstidno, string nationality, string description, int user_id)
        {
            this.user_id = user_id;
            this.name = name;
            this.phone = phone;
            this.guesttype = guesttype;
            this.count = count;
            this.cstidno = cstidno;
            this.nationality = nationality;
            this.description = description;
        }
        public int user_id;
        public string name;
        public string phone;
        public int guesttype;
        public int count;
        public string cstidno;
        public string nationality;
        public string description;
 
    }
}
