﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
   public class User
    {
        public User(string username,string password,string password_confirmation)
        {
            this.username = username;
            this.password = password;
            this.password_confirmation = password_confirmation;
        }
        public  int id { get; set; }
        public string username { get; set; }
        public string auth_token;
        public string password;
        public string password_confirmation;
        public string password_digest;
        public int usergroup_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}

