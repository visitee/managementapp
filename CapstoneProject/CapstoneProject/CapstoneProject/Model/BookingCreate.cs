﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class BookingCreate
    {
        public BookingCreate(int status, int room_id, int guest_id)
        {
            this.status = status;
            this.guest_id = guest_id;
            this.room_id = room_id;
        }

        public int status;
        public int guest_id;
        public int room_id;
    }
}
