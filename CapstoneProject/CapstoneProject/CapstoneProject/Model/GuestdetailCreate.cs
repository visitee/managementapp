﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class GuestdetailCreate
    {
        public GuestdetailCreate(   float prepaidmoney, string creditcard,  string description, int billing_id, int guest_id)
        { 
        this.billing_id = billing_id;
        this.guest_id = guest_id;
        this.prepaidmoney = prepaidmoney;
        this.creditcard = creditcard;
        this.description = description;

    }
     
        public int billing_id;
        public int guest_id ;
        public float prepaidmoney ;
        public string creditcard ;
        public string description ; 
    }
}
