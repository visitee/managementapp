﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class ServiceOrder
    {
        public int id { get; set; } 
        public int quantity { get; set; }
        public int guest_id { get; set; }
        public int service_id { get; set; }
        public string description { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; } 
    }
}
