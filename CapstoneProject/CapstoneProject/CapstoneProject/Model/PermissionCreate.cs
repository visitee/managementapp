﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class PermissionCreate
    {
        public PermissionCreate(int usergroup_id, int feature_id, string description) {
            this.usergroup_id = usergroup_id;
            this.feature_id = feature_id;
            this.description = description;
        }

        public int usergroup_id { get; set; }
        public int feature_id { get; set; }
        public string description { get; set; }
    }
}
