﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class EmployeeCreate
    {
        //rfidcard_id, description, job_id, department_id
        public EmployeeCreate(int user_id, string name, string gender, string phone, int rfidcard_id, string description, int job_id, int department_id)
        {
            this.user_id = user_id;
            this.name = name;
            this.gender = gender;
            this.phone = phone;
            this.rfidcard_id = rfidcard_id;
            this.job_id = job_id;
            this.description = description;
            this.department_id = department_id;


        }

        public int user_id;
        public string name;
        public string gender;
        public string phone;
        public int rfidcard_id;
        public int job_id;
        public string description;
        public int department_id;
    }

}
