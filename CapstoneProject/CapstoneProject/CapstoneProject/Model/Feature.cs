﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class Feature
    {
        public int id { get; set; }
        public string namevn { get; set; }
        public string nameen { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public bool isChecked { get; set; }

    }
}
