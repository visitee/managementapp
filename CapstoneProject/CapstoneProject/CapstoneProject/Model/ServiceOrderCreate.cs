﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class ServiceOrderCreate
    {
        public ServiceOrderCreate(int guest_id,int service_id, int quantity, string description  )
        { 
        this.quantity = quantity;
        this.guest_id  = guest_id;
        this.service_id  = service_id;
        this.description  = description; 
    } 
        public int quantity;
        public int guest_id ;
        public int service_id ;
        public string description ; 
    }
}
