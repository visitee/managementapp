﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class Token
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
       public string token { get; set; }
        public DateTime expries { get; set; }
        public int userid { get; set; }
        public int usergroupid { get; set; }
        public string jsonpermission { get; set; }
    }
}
