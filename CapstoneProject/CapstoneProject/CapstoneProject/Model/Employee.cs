﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class Employee : INotifyPropertyChanged
    {
        private int _id;
        public int id
        {
            get
            {
                return this._id;
            }
            set
            {
                if (this._id != value)
                {
                    this._id = value;
                    this.OnPropertyChanged("id");
                }
            }
        }

        private int _user_id;
        public int user_id
        {
            get
            {
                return _user_id;
            }
            set
            {
                if (this._user_id != value)
                {
                    this._user_id = value;
                    this.OnPropertyChanged("user_id");
                }
            }
        }
        private string _name = string.Empty;
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                if (this._name != value)
                {
                    this._name = value;
                    this.OnPropertyChanged("name");
                }
            }
        }
        
        public DateTime created_at{ get; set; }
        public DateTime updated_at { get; set; }

        private string _gender = string.Empty;
        public string gender
        {
            get { return _gender; }
            set { if (this._gender != value) { this._gender = value; this.OnPropertyChanged("gender"); } }
        }
        private string _phone = string.Empty;
        public string phone
        {
            get { return _phone; }
            set { if (this._phone != value) { this._phone = value; this.OnPropertyChanged("phone"); } }
        }
        public int rfidcard_id { get; set; }
        public int job_id { get; set; }
        private string _description = string.Empty;
        public string description
        {
            get { return _description; }
            set { if (value!=null&&this._description != value) { this._description = value; this.OnPropertyChanged("description"); } }
        }
        public int department_id { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
