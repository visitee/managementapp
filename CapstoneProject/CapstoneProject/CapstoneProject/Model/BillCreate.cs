﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class BillCreate
    {
        public BillCreate(float valuevnd, float valueusd, int status, int overtime, float overvaluevnd, float overvalueusd, int guest_id)
        {
        
            this.valuevnd = valuevnd;
        this.valueusd = valueusd;
        this.status = status;
        this.overtime = overtime;
        this.overvaluevnd = overvaluevnd;
        this.overvalueusd = overvalueusd;
        this.guest_id = guest_id;
    }
    public float valuevnd;
    public float valueusd;
    public int status;
    public int overtime;
    public float overvaluevnd;
    public float overvalueusd;
    public int guest_id;
}
}
