﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class RoomCreate
    {
        public RoomCreate(string no, string name, int status, string description, int floor_id, int roomtype_id)
        {
        this.no = no;
        this.name = name;
        this.floor_id = floor_id;
        this.status = status;
        this.description = description;
        this.roomtype_id = roomtype_id;
    }

    public string no;
    public string name;
    public int floor_id;
    public int status;
    public string description;
    public int roomtype_id;
}
}
