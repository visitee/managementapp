﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class Room
    {
        public int id { get; set; }
        public string no { get; set; }
        public string name { get; set; }
        public int floor_id { get; set; }
        public int roomtype_id { get; set; }
        public int device_id { get; set; }
        public int status { get; set; }
        public string pin { get; set; }
        public string pinvalue { get
            {
                try {
                    if (pin == null)
                        return "";
                    double value = Math.Round(double.Parse(pin) / 3.5*100,2);
                    return "pin: "+value+"%";
                }
                catch {
                    return "";
                }
            } }
        public string description { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public string image { get; set; }
        public string color { get; set; }
        public DateTime firstcheckin { get; set; }
        public DateTime lastcheckin { get; set; }
    }
}
