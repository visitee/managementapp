﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class CheckInOut
    {
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int rfidcard_id;
        public int room_id;
    }
}
