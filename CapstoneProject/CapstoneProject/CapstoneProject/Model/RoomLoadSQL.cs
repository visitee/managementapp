﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class RoomLoadSQL
    {
        public int id { get; set; }
        public string no { get; set; }
        public string name { get; set; } 
        public int status { get; set; }
        public string description { get; set; } 
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
        public int floor_id { get; set; }
        public int roomtype_id { get; set; }
        public int device_id { get; set; } 
    }
}
