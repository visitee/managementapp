﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class RfidCard
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int room_id { get; set; }
        public string room { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}
