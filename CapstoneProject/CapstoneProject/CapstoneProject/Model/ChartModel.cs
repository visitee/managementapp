﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject
{
    public class ChartModel
    {
        public string Name { get; set; }

        public DateTime date { get; set; }

        public double Value { get; set; }

        public double Value1 { get; set; }

        public double Size { get; set; }

        public double High { get; set; }

        public double Low { get; set; }

        public ChartModel(string name, double value)
        {
            Name = name;
            Value = value;
        }

        public ChartModel(string name, double value, double size)
        {
            Name = name;
            Value = value;
            Size = size;
        }

        public ChartModel(double value, double value1, double size)
        {
            Value1 = value;
            Value = value1;
            Size = size;
        }

        public ChartModel(string name, double high, double low, double open, double close)
        {
            Name = name;
            High = high;
            Low = low;
            Value = open;
            Size = close;
        }
        public ChartModel(DateTime Date, double high, double low, double open, double close)
        {
            date = Date;
            High = high;
            Low = low;
            Value = open;
            Size = close;
        }
        public ChartModel(double value, double size)
        {
            Value = value;
            Size = size;
        }

        public ChartModel(DateTime dateTime, double value)
        {
            date = dateTime;
            Value = value;
        }
    }

    public class ViewModel
    {
        public ObservableCollection<ChartModel> CircularData { get; set; }
        public ObservableCollection<ChartModel> datas1 { get; set; }

        public ViewModel()
        {
            CircularData = new ObservableCollection<ChartModel>
            {
                new ChartModel("2010", 8000),
                new ChartModel("2011", 8100),
                new ChartModel("2012", 8250),
                new ChartModel("2013", 8600),
                new ChartModel("2014", 8700)
            };

            datas1 = new ObservableCollection<ChartModel>
            {
                new ChartModel("2010", 6),
                new ChartModel("2011", 15),
                new ChartModel("2012", 35),
                new ChartModel("2013", 65),
                new ChartModel("2014", 75)
            };


        }
    }

}
