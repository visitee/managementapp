﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class Billing
    {
        public int id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime update_at { get; set; }
        public float valuevnd { get; set; }
        public float valueusd { get; set; }
        public int status { get; set; }
        public int overtime { get; set; }
        public float overvaluevnd { get; set; }
        public float overvalueusd { get; set; }
        public int guest_id { get; set; }
    }
}
