﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class ServiceCreate
    {
        public ServiceCreate( string name ,string code, float price, string description )
        {
            this.code = code;
        this.name = name;
            this.price = price;
        this.description = description;
    }

        public string name { get; set; }
        public string code { get; set; }
        public float price { get; set; }
        public string description { get; set; }

    }
}
