﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class UserCreate
    {
        public UserCreate(string username,string password,string password_confirmation,int usergroup_id)
        {
            this.usergroup_id = usergroup_id;
            this.username = username;
            this.password = password;
            this.password_confirmation = password_confirmation;
        }
        public string username;
        public string password;
        public string password_confirmation;
        public int usergroup_id;
    }
}
