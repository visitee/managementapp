﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class ServiceNew
    {
        public ServiceNew(string name, string code, float price, string description)
        {
            this.name = name;
            this.code = code;
            this.price = price;
            this.description = description;

        }

        public string name;
        public string code;
        public float price;
        public string description;
    }
}
