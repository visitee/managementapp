﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class Guest
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public int guesttype { get; set; }
        public int count { get; set; }
        public string cstidno { get; set; }
        public string nationality { get; set; }
        public string description { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }


}
