﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapstoneProject.Model
{
    public class RoomType
    {
        public int id { get; set; }
        public string name { get; set; }
        public string namevn { get; set; }
        public string description { get; set; }
        public int base_rate { get; set; }
        public int extra_rate { get; set; }
        public int fullday_rate { get; set; }
        public int night_rate { get; set; }
        public int special_rate { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}
