﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CapstoneProject
{
    public partial class Menu : ContentPage
    {

        public ListView MenuList
        {
            get { return listview; }
            set { listview = value; }
        }

        public Menu()
        {
            InitializeComponent();
            listview.ItemsSource= new MenuListData();
        }
    }
}
