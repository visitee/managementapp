﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using CapstoneProject;
using CapstoneProject.Pages;

namespace CapstoneProject
{

	public class MenuListData : List<MenuItem>
	{
		public MenuListData ()
		{
			this.Add (new MenuItem () { 
				Title = "Danh sách phòng", 
				IconSource = "listroom.png", 
				TargetType = typeof(Main)
			});

			this.Add (new MenuItem () { 
				Title = "Báo cáo tài chính", 
				IconSource = "finan.png", 
				TargetType = typeof(FinancialReportXamarin)
			});

			this.Add (new MenuItem () {
				Title = "Mở cửa",
				IconSource = "DoorOpen.png",
				TargetType = typeof(OpenDoor)
			});
            this.Add(new MenuItem()
            {
                Title = "Cấu hình thẻ",
                IconSource = "config.png",
                TargetType = typeof(ConfigCard)
            });

            this.Add(new MenuItem()
            {
                Title = "Đăng xuất",
                IconSource = "logout.png",
                TargetType = typeof(Login)
            });
        }
	}
}