﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Syncfusion.SfChart.XForms.iOS.Renderers;
using Syncfusion.SfChart.iOS;
using Syncfusion.SfChart.XForms;

namespace CapstoneProject.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            App.ScreenWidth = UIScreen.MainScreen.Bounds.Width;

            new SfChartRenderer();

            /*new SfBusyIndicatorRenderer();

            new SfBarcodeRenderer();

            new SfScheduleRenderer();

            new SfDigitalGaugeRenderer();

            new SfLinearGaugeRenderer();

            new SfRangeSliderRenderer();

            new SfMapsRenderer();

            new SfAutoCompleteRenderer();

            new SfNumericTextBoxRenderer();

            new SfTreeMapRenderer();

            new SfRatingRenderer();

            new SfCalendarRenderer();

            SfDataGridRenderer.Init();
            */

            LoadApplication(new App());

            Xamarin.Forms.Forms.ViewInitialized += (object sender, Xamarin.Forms.ViewInitializedEventArgs e) =>
            {
                if (e.NativeView is SFChart)
                {
                    if (e.View.StyleId == "Tooltip")
                    {
                        (e.NativeView as SFChart).Delegate = new TooltipCustomDelegate(e.View as SfChart);
                    }
                }
            };

            app.StatusBarHidden = false;
            app.StatusBarStyle = UIStatusBarStyle.LightContent;

            return base.FinishedLaunching(app, options);
        }
    }
}
