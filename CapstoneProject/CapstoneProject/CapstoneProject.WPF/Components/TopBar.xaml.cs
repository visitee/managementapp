﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using CapstoneProject.WPF.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for TopBar.xaml
    /// </summary>
    public partial class TopBar : UserControl
    {
        public TopBar()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).animatemenu();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Session.ActiveSession.Logout();
        }

        private void Notification_Click(object sender, RoutedEventArgs e)
        {
            if(e.RoutedEvent.Equals(Mouse.MouseMoveEvent))
                ((MainWindow)App.Current.MainWindow).setNotifyVisibility(Visibility.Visible);
            else 
                ((MainWindow)App.Current.MainWindow).setNotifyVisibility(Visibility.Collapsed);
        }

        public async void setTopBar()
        {
            stackpanel.Visibility = Visibility.Visible;
           VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            try
            {
                var user = await vc.GetAsync<User>("users/"+Session.ActiveSession.CurrentAccessTokenData.userId);
                this.DataContext = user;
            }
            catch { }
        }
        
        public void collapsedTopBar()
        {
            stackpanel.Visibility = Visibility.Collapsed;
        }
    }
}
