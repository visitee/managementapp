﻿using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for NotificationGUI.xaml
    /// </summary>
    public partial class NotificationGUI : UserControl
    {
        public NotificationGUI()
        {
            InitializeComponent();
        }

        public void setBinding(List<Room> listroom) {
            foreach(var room in listroom) {
                noti.Add(new MappingName() { Header = room.name, Name = Properties.Resources.NotificationMessage });
            }
            listview.ItemsSource = noti;
        }

        ObservableCollection<MappingName> noti = new ObservableCollection<MappingName>();
    }
}
