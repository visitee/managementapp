﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using CapstoneProject.WPF.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for LeftSmallMenu.xaml
    /// </summary>
    public partial class LeftSmallMenu : UserControl
    {
        public LeftSmallMenu()
        {
            InitializeComponent();
        }

        public void setMenu(List<Feature> features)
        {
            foreach (var feature in features)
            {
                if (checkFeaturePermission(feature))
                { 
                    Button button = new Button();
                    button.Tag = feature.id;

                    button.Height = 40;

                    button.Style = (Style)App.Current.Resources[Properties.Resources.ButtonMenuStyle];
                    button.DataContext = feature;
                    button.Click += Button_Click;
                    stackpanel.Children.Add(button);
                }
            }
        }

        private bool checkFeaturePermission(Feature input)
        {
            var list =Session.ActiveSession.CurrentAccessTokenData.CurrentPermissions;
            foreach(var permission in list)
            {
                if (permission.feature_id.Equals(input.id))
                    return true;
            }
            return false;
        }

        public void clearMenu()
        {
            stackpanel.Children.Clear();
        }

        public int chilcount { get {
                if (stackpanel.Children != null)
                    return stackpanel.Children.Count;
                else return 0;
            } }
        private AddNewUser addnewuser;
        private ListTable listemployee;
        private ListRoom listroom;
        private Employees employees;
        private ListRoomInfo listRoomInfo;
        private GuestNew guestNew;
        private ListGuestTable listGuest;
        private BookingRoom bookingRoom;
        private RoomNew newRoom;
        private FinancialReport financialReport;
        private ListService listService;
        private ServiceCreate serviceCreate;
        private CheckInLogReport checkInLogReport;
        private ListBills listBill;
        private CheckOut checkOut;
        private UserNew newUser;
        private GroupUserNew newUsergroup;
        private SetPermition setPermition;
        private ServiceOrderNew serOrderNew;
        private ChangeRoom changeroom;
        private AdminManagementCenter adminPage;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            enableAllButton();
            ((Button)sender).IsEnabled = false;

            //  Room status 1
            if ((int)((Button)sender).Tag == 1)
            {
                listroom = new ListRoom();
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(listroom); 
            } 
            // Booking Room 2
            else if ((int)((Button)sender).Tag == 2)
            {
                bookingRoom = new BookingRoom();
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(bookingRoom);
            } 
            // CheckOut 3  // Chang Room 4 
            else if ((int)((Button)sender).Tag == 3)
            {
                checkOut = new CheckOut();
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(checkOut);
            }
            // Table list: employee // room // Bill // asset vs product // service order // Guest // Job // Department //  
            else if ((int)((Button)sender).Tag == 4)
            {
                listemployee = new ListTable();
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(listemployee);
            }
            // Admin management 5:  Work Schedule // config equipment // Permission //  User Management 
            else if ((int)((Button)sender).Tag == 5)
            {
                adminPage = new AdminManagementCenter();
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(adminPage);
            }
            // Report 7 in-ot
            else if ((int)((Button)sender).Tag == 7)
            {
                checkInLogReport = new CheckInLogReport();
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(checkInLogReport);
            }
            // Report 6 finance
            else if ((int)((Button)sender).Tag == 6)
            {
                financialReport = new FinancialReport();
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(financialReport);
            }
            // Help 8
            else if ((int)((Button)sender).Tag == 8)
            { 
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new Help());
            }
            // About 9
            else if ((int)((Button)sender).Tag == 9)
            { 
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                if (!((MainWindow)App.Current.MainWindow).isMenuCollapse())
                    ((MainWindow)App.Current.MainWindow).animatemenu();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new About());
            }

        }

        private void enableAllButton()
        {
            foreach(UIElement a in stackpanel.Children) {
                if (a.GetType().Equals(typeof(Button)))
                    ((Button)a).IsEnabled = true;        
            }
        }
    }
}
