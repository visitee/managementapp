﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ListGuestTable.xaml
    /// </summary>
    public partial class ListGuestTable : Page
    {
        public ListGuestTable()
        {
            try

            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                // Log error (including InnerExceptions!)
                // Handle exception
                System.Console.WriteLine(ex.ToString());
            }
            this.Loaded += ListGuest_Loaded;

        }

        private async void ListGuest_Loaded(object sender, RoutedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            try
            {
                var listem = await vc.GetAsync<List<Guest>>("guests");
                sfGrid_guestList.ItemsSource = listem;


                var viewModel = this.DataContext as EntityInfoViewModel;
                viewModel.filterChanged = OnFilterChanged;
            }
            catch { vc.cancelPendingRequests(); }



        }

        private void OnFilterChanged()
        {
            try
            {
                var viewModel = this.DataContext as EntityInfoViewModel;
                if (sfGrid_guestList.View != null)
                {
                    sfGrid_guestList.View.Filter = viewModel.FilerRecords;
                    sfGrid_guestList.View.RefreshFilter();
                }
            }
            catch (Exception)
            { 
            }
            
        }

        private void sfGrid_SelectionChanged(object sender, Syncfusion.UI.Xaml.Grid.GridSelectionChangedEventArgs e)
        {
            var a = sfGrid_guestList;
        }

        private void Bill_btn_AddGuest_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new GuestNew());
        }
    }
}
