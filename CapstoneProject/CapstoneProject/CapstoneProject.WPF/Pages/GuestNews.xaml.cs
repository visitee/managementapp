﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for GuestNew.xaml
    /// </summary>
    public partial class GuestNew : Page
    {
        FormType ft = FormType.Add;
        Guest guest;
        
        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        User current;
        public GuestNew()
        {
            InitializeComponent();
            try
            {
                if ((bool)Session.Parameters["isEdit"])
                {
                    ft = FormType.Edit;
                    var user = Session.Parameters["guest"];
                    guest = (Guest)user;
                    //code xu ly
                    Session.Parameters.Remove("guest");
                    //  this.Loaded += updateLoaded;
                }
                this.Loaded += AddNewEmployee_Loaded;
            }
            catch
            {
                this.Loaded += AddNewEmployee_Loaded;
            }

            //this.Loaded += AddNewGuest_Loaded;
             

        }


        protected async void AddNewEmployee_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                current = await vc.GetAsync<User>("users/" + Session.ActiveSession.CurrentAccessTokenData.userId);
                GuestNew_cbbox_nationality.Items.Add("VietNam");
                GuestNew_cbbox_nationality.Items.Add("Ko Biet");
                GuestNew_cbb_type.Items.Add("1");
                GuestNew_cbb_type.Items.Add("2");
                GuestNew_cbb_type.Items.Add("3");

                if (ft.Equals(FormType.Add))
                {
                    GuestNew_tbox_UserID.Text = current.username;
                    GuestNew_tbox_UserID.IsEnabled = false;
                    GuestNew_Btn_Update.IsEnabled = false;
                }
                if (ft.Equals(FormType.Edit))
                {
                    GuestNew_Btn_SaveAll.IsEnabled = false;
                    GuestNew_tbox_UserID.Text = guest.user_id + "";
                    GuestNew_tbox_UserID.IsEnabled = false;
                    GuestNew_tbox_CusID.Text = guest.cstidno;
                    GuestNew_tbox_CusID.IsEnabled = false;
                    GuestNew_tbox_name.Text = guest.name;
                    GuestNew_tbox_name.IsEnabled = false;
                    GuestNew_tbox_phone.Text = guest.phone;
                    GuestNew_tbox_count.Text = guest.count + "";
                    GuestNew_tbox_description.Text = guest.description;

                    /*   foreach (int item in GuestNew_cbb_type.Items)
                       {
                           if (item == guest.guesttype)
                               GuestNew_cbb_type.SelectedItem = item;
                           break;
                       }*/
                    foreach (string item in GuestNew_cbbox_nationality.Items)
                    {
                        if (item == guest.nationality)
                            GuestNew_cbbox_nationality.SelectedItem = item;
                        break;
                    }

                }
                if (ft.Equals(FormType.View))
                {

                }
            }
            catch (Exception)
            { 
            }
            

        }
         

        private async void GuestNew_Btn_SaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //System.Console.WriteLine("xxx" + Employee_tbox_name.Text);
                int count, type;
                string name, phone, cstidno, nationality, desc;

                // user_id = Convert.ToInt32(GuestNew_tbox_UserID.Text);
                count = Convert.ToInt32(GuestNew_tbox_count.Text);
                type = Convert.ToInt32(GuestNew_cbb_type.SelectedItem.ToString());
                name = GuestNew_tbox_name.Text;
                cstidno = GuestNew_tbox_CusID.Text;
                phone = GuestNew_tbox_phone.Text;
                nationality = GuestNew_cbbox_nationality.SelectedItem.ToString();
                desc = GuestNew_tbox_description.Text;


                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                GuestCreate uc = new GuestCreate(name, phone, type, count, cstidno, nationality, desc, current.id);
                Guest guestNew = JsonConvert.DeserializeObject<Guest>(await vc.PostAsync(uc));

                GuestNew_tbox_CusID.Text = guestNew.id.ToString();
            }
            catch (Exception)
            { 
            }
            
        }

        private void GuestNew_Btn_GoBack_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }

        private void GuestNew_Btn_Update_Click(object sender, RoutedEventArgs e)
        {
            int phoneNo;
            try
            {
                phoneNo = Convert.ToInt32(GuestNew_tbox_phone.Text);
                if (phoneNo == null || phoneNo > 99999999999 || phoneNo < 100000000)
                {
                    // Please select group id
                    MessageBox.Show("Please check phone number 'phoneNo > 99999999999 || phoneNo < 100000000' !");
                    return;
                }
                guest.phone = phoneNo+"";
            }
            catch (Exception)
            {
                MessageBox.Show("Please check phone number 'phoneNo > 99999999999 || phoneNo < 100000000' !");
                return;
            }
            int countNo;
            try
            {
                countNo = Convert.ToInt32(GuestNew_tbox_count.Text);
                if (countNo == null || 0 > countNo)
                {
                    // Please select group id
                    MessageBox.Show("Please check countNo number 'countNo > 0' !");
                    return;
                }
                guest.count = countNo;
            }
            catch (Exception)
            {
                MessageBox.Show("Please check countNo number 'countNo > 0' !");
                return;
            }
            try
            {
                guest.description = GuestNew_tbox_description.Text;
                 
            }
            catch (Exception ex)
            {
                MessageBox.Show("Input Error !\n" + ex);
            }

            try
            {
               // VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                vc.UpdateAsync<Guest>(guest, guest.id);
                MessageBox.Show("Guest is updated !");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Guest is not updated !\n");
            }


        }
    }
}
