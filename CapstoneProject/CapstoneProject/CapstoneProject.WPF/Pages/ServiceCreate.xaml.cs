﻿using CapstoneProject.Classes; 
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ServiceCreate.xaml
    /// </summary>
    public partial class ServiceCreate : Page
    {
        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        //List<Service> serviceList = await vc.GetAsync<List<Service>>("services");

        FormType ft = FormType.Add;
        Service servicexxx;
        public ServiceCreate()
        {
            InitializeComponent();
            try
            {
                if ((bool)Session.Parameters["isEdit"])
                {
                    ft = FormType.Edit;
                    var user = Session.Parameters["service"];
                    servicexxx = (Service)user;
                    //code xu ly
                    Session.Parameters.Remove("service");
                    //  this.Loaded += updateLoaded;
                }
                this.Loaded += AddNewEmployee_Loaded;
            }
            catch
            {
                this.Loaded += AddNewEmployee_Loaded;
            }


        }

        protected async void AddNewEmployee_Loaded(object sender, RoutedEventArgs e)
        { 
            Service_cbb_deprate.Items.Add("100 %");
            Service_cbb_deprate.Items.Add("80 %");
            Service_cbb_deprate.Items.Add("70 %");
 

            if (ft.Equals(FormType.Add))
            {
                Service_btn_update.IsEnabled = false;
                //    Employee_tbox_UserID.Text = 
                // MessageBox.Show("Add!");
            }
            if (ft.Equals(FormType.Edit))
            {
                Service_btn_new.IsEnabled = false;
                // MessageBox.Show("Edit!");
                Service_tb_id.Text = servicexxx.id.ToString();
                Service_tb_id.IsEnabled = false;
                Service_tb_name.Text = servicexxx.name;
                Service_tb_name.IsEnabled = false;
                Service_tb_code.Text = servicexxx.code;
                Service_tb_desc.Text = servicexxx.description;
                Service_db_price.Text = servicexxx.price.ToString();

              /*  foreach (Department item in Employee_cbbox_department.Items)
                {
                    if (item.id == empe.department_id)
                        Employee_cbbox_department.SelectedItem = item;
                    break;
                } */

            }
            if (ft.Equals(FormType.View))
            {

            }

        }

        private async void Service_btn_update_Click(object sender, RoutedEventArgs e)
        {
            /*float price = 0;
            int depRate = 100;
            string name, code, desc;

            price = (float)Convert.ToDouble(Service_db_price.Text);
            name = Service_tb_name.Text;
            code = Service_tb_code.Text;
            desc = Service_tb_desc.Text;

            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            ServiceNew sn = new ServiceNew(name, code, price, desc);
            int i = await vc.UpdateAsync(sn,1);
            */
            float price;
            try
            {
                price = (float) Convert.ToDouble(Service_db_price.Text);
                if (price == null || price < 0.0 )
                { 
                    MessageBox.Show("Please check price number 'price > 0.0' !");
                    return;
                }
                servicexxx.price = price;
            }
            catch (Exception)
            {
                MessageBox.Show("Please check price number 'price > 0.0' !");
                return;
            }
            string code;
            try
            {
                code = Service_tb_code.Text ;
                if (code == null  )
                {
                    MessageBox.Show("Please check code not Null !");
                    return;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please check code not Null !");
                return;
            }

             servicexxx.description = Service_tb_desc.Text;
             

            try
            {
                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                vc.UpdateAsync<Service>(servicexxx, servicexxx.id);
                MessageBox.Show("Service is updated !");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Service is updated !");
                ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
            }

        }

        private async void Service_btn_new_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                float price = 0;
                int depRate = 100;
                string name, code, desc;

                price = (float)Convert.ToDouble(Service_db_price.Text);
                name = Service_tb_name.Text;
                code = Service_tb_code.Text;
                desc = Service_tb_desc.Text;

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                ServiceNew sn = new ServiceNew(name, code, price, desc);
                string xxx = await vc.PostAsync(sn);
            }
            catch (Exception)
            { 
            }
            
             
        }

        private void Service_btn_back_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }
    }
}
