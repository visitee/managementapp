﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for GroupUserNew.xaml
    /// </summary>
    public partial class GroupUserNew : Page
    {
        public GroupUserNew()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
            }

            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            this.Loaded += AddNewUserLoaded;
        }


        //private FormType 
        FormType ft = FormType.Add;

        protected async void AddNewUserLoaded(object sender, RoutedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
             

        }

        private async void UsergroupNew_Btn_SaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string usergroup, desc;

                usergroup = UsergroupNew_tbox_usergroup.Text;
                desc = UsergroupNewe_tbox_desc.Text;

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                UsergroupCreate uc = new UsergroupCreate(usergroup, desc);
                var xxx = await vc.PostAsync(uc);

            }
            catch (Exception)
            { 
            }
           
        }

        private async void UsergroupNew_Btn_UpdateAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string usergroup, desc;

                usergroup = UsergroupNew_tbox_usergroup.Text;
                desc = UsergroupNewe_tbox_desc.Text;

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                UsergroupCreate uc = new UsergroupCreate(usergroup, desc);
                var xxx = await vc.PostAsync(uc);

            }
            catch (Exception)
            {
            }
        }

        private void UsergroupNew_Btn_Back_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }
    }
}
