﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for BookingRoom.xaml
    /// </summary>
    public partial class BookingRoom : Page
    {

        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        List<RoomType> roomtypeList = new List<RoomType>();
        List<Room> roomsList = new List<Room>();
        List<Booking> bookingList = new List<Booking>();

        Room roomSelected = new Room();
        RoomType roomTypeSelected = new RoomType();
        User current;



        public BookingRoom()
        {
            try
            {
                InitializeComponent();
                CheckIn_cbb_Nationality.Items.Add("VietNam");
                CheckIn_cbb_Nationality.Items.Add("UnKnown");

                this.Loaded += AddNewBooking_Loaded;
            }
            catch (Exception ex)
            {
               // MessageBox.Show("loadDataToTable Error!!!\n"+ex);
              //  return;
            }

            
            
        }

        private async void loaddatatotable()
        {
            this.Dispatcher.Invoke((Action)(async () =>
            {
                roomsList = await vc.GetSqlAsync<List<Room>>("Select * from rooms where status=1");
                CheckIn_cbb_room.ItemsSource = roomsList;
                sfGrid.ItemsSource = roomsList;
                //columnCombo.ItemsSource = roomsList;
                roomtypeList = await vc.GetAsync<List<RoomType>>("roomtypes");
                CheckIn_cbb_booktype.ItemsSource = roomtypeList;

            }));

        }



        // do dlieu vao Room, click checkin -> nwe Guest, new bill, new booking, guestdetails

        protected async void AddNewBooking_Loaded(object sender, RoutedEventArgs e)
        {
            current = await vc.GetAsync<User>("users/" + Session.ActiveSession.CurrentAccessTokenData.userId);

            try
            {
                Thread loadDataToTable = new Thread(loaddatatotable);
                loadDataToTable.Start();
                 
                CheckIn_tb_staffID.Text = current.username;
                CheckIn_tb_staffID.IsEnabled = false;
            }
            catch (Exception ex) {
               // MessageBox.Show("loadDataToTable Error!!!\n" + ex);
               // return;
            }

        }
         

        private void reloadView()
        {
            CheckIn_tb_cusName.Text = "";
            CheckIn_tb_desc.Text = "";
            CheckIn_tb_guestCount.Text = 0 + "";
            CheckIn_tb_identityID.Text = 0 + "";
            CheckIn_tb_phone.Text = 0 + "";
            CheckIn_tb_guestAcc.Text = "";
        }
        private async void CheckIn_btn_CHECKIN_Click(object sender, RoutedEventArgs e)
        {
            Guest thisGuest = new Guest();
            Billing thisBill = new Billing();
            Booking thisBooking = new Booking();
            Room thisRoom = new Room();
            string name, rfid, description;
            int cstdio, phoneNo, count2, guesttype ;

            #region validate
            var selectedRoom = CheckIn_cbb_room.SelectedValue;
            if (selectedRoom == null)
            {
                // Please select group id
                MessageBox.Show("Please select Room to CheckIn !");
                return;
            }
            var selectedBookingType = CheckIn_cbb_booktype.SelectedValue;
            if (selectedBookingType == null)
            {
                // Please select group id
                MessageBox.Show("Please select Booking Type to CheckIn !");
                return;
            }

            try
            {
                name = CheckIn_tb_cusName.Text;
                if (name == null || name.Length < 4)
                {
                    // Please select group id
                    MessageBox.Show("Please check name Input, length > 4 !");
                    return;
                }
                thisGuest.name = name;

            }
            catch (Exception)
            {
                MessageBox.Show("Please check name !");
                return;
            }
 
            try
            {
                cstdio = Convert.ToInt32(CheckIn_tb_identityID.Text);
                if (cstdio == null)
                {
                    // Please select group id
                    MessageBox.Show("Please check Identity number  !");
                    return;
                }
                thisGuest.cstidno = cstdio+"";
            }
            catch (Exception)
            {
                MessageBox.Show("Please check Identity number !");
                return;
            }

            try
            {
                phoneNo = Convert.ToInt32(CheckIn_tb_phone.Text);
                if (phoneNo == null)
                {
                    // Please select group id
                    MessageBox.Show("Please check phone number !");
                    return;
                }
                thisGuest.phone = phoneNo+"";
            }
            catch (Exception)
            {
                MessageBox.Show("Please check phone number !");
                return;
            }
              
            try
            {
                count2 = Convert.ToInt32(CheckIn_tb_guestCount.Text);
                if (count2 == null || count2 < 0 || count2 > 100)
                {
                    // Please select group id
                    MessageBox.Show("Please check Count number  !");
                    return;
                }
                thisGuest.count = count2;
            }
            catch (Exception)
            {
                MessageBox.Show("Please check Count number !");
                return;
            }
            #endregion



           
              
            // create Guest new  
            try
            {
                #region Create Guest New
                thisGuest.description = CheckIn_tb_desc.Text;
                thisGuest.guesttype = 1;

                if (CheckIn_cbb_Nationality.SelectedIndex == 0) thisGuest.nationality = "VietNam"; else thisGuest.nationality = "UnKnown";  
                GuestCreate uc = new GuestCreate(thisGuest.name, thisGuest.phone+ "", thisGuest.guesttype, thisGuest.count, thisGuest.cstidno + "", thisGuest.nationality, thisGuest.description, current.id);
                var guestnew = JsonConvert.DeserializeObject<Guest>(await vc.PostAsync<GuestCreate>(uc));
                #endregion

                #region bill create
                // create Bill theo Guest_ID  
                int bill_status = 2; // chua thanh toan
                BillCreate bc = new BillCreate(0, 0, bill_status, 0, 0, 0, guestnew.id);
                var billNew = JsonConvert.DeserializeObject<Billing>(await vc.PostAsync<BillCreate>(bc));
                #endregion

                // create GuestDetail theo Guest_ID, Billing_ID
                GuestdetailCreate gtc = new GuestdetailCreate(0, "", "", billNew.id, guestnew.id);
                var guestdetailNew = JsonConvert.DeserializeObject<Guestdetail>(await vc.PostAsync<GuestdetailCreate>(gtc));
                  
                //  create Booking theo Guest_ID, Room_ID
                int booking_status = 2; // chua thanh toan
                BookingCreate bbc = new BookingCreate(booking_status, roomSelected.id, guestnew.id);
                var bookingNew = JsonConvert.DeserializeObject<Booking>(await vc.PostAsync<BookingCreate>(bbc));

                // update room status
                roomSelected.status = 3; // booking nhung chua dung
                vc.UpdateAsync<Room>(roomSelected, roomSelected.id);

                //reloadView();
                CheckIn_tb_billId.Text = billNew.id.ToString();

                Thread loadDataToTable2 = new Thread(loaddatatotable);
                loadDataToTable2.Start();

                MessageBox.Show("CheckIn is successfully !\n" ); 
                ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack(); 
            }
            
            catch (Exception ex)
            {
                MessageBox.Show("Please select Check Your Input to CheckIn !\n"+ ex);
                return;
            }
        }

        private void CheckIn_btn_ORDER_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new ServiceOrderNew());
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();

        }

        private void CheckIn_btn_addCustomer_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new GuestNew());
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
        }


 
        private void CheckIn_cbb_room_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            { var a = (ComboBox)sender;
                roomSelected = (Room)a.SelectedItem;
            }
            catch (Exception)
            {
               
            }

        }

        private void CheckIn_cbb_booktype_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var a = (ComboBox)sender;
                roomTypeSelected = (RoomType)a.SelectedItem;
            }
            catch (Exception)
            { 
            }

        }

        private void Emple_btn_save22_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }
    }
}



