﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for Employees.xaml
    /// </summary>
    /// 

    public partial class Employees : Page
    {
        FormType ft = FormType.Add;
        Employee empe;
        public Employees()
        {
            InitializeComponent();
            try
            {
                if ((bool)Session.Parameters["isEdit"])
                {
                    ft = FormType.Edit;
                    var user = Session.Parameters["employee"];
                    empe = (Employee)user;
                    //code xu ly
                    Session.Parameters.Remove("employee");
                  //  this.Loaded += updateLoaded;
                }
                this.Loaded += AddNewEmployee_Loaded;
            }
            catch
            {
                this.Loaded += AddNewEmployee_Loaded;
            }
            
        }
         
        protected async void AddNewEmployee_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                List<Employee> employeesList = await vc.GetAsync<List<Employee>>("employees");
                List<Department> departmentsList = await vc.GetAsync<List<Department>>("departments");
                List<Job> jobsList = await vc.GetAsync<List<Job>>("jobs");

                Employee_cbbox_jobId.ItemsSource = jobsList;
                Employee_cbbox_department.ItemsSource = departmentsList;

                Employee_cbb_gender.Items.Add("Male");
                Employee_cbb_gender.Items.Add("Female");


                if (ft.Equals(FormType.Add))
                {
                    Employee_Btn_update.IsEnabled = false;
                    //    Employee_tbox_UserID.Text = 
                    // MessageBox.Show("Add!");
                }
                if (ft.Equals(FormType.Edit))
                {
                    // MessageBox.Show("Edit!");
                    Employee_tbox_name.Text = empe.name;
                    Employee_tbox_name.IsEnabled = false;
                    Employee_tbox_description.Text = empe.description;
                    Employee_tbox_phone.Text = empe.phone;
                    Employee_tbox_rfidCard.Text = empe.rfidcard_id + "";
                    //  Employee_IntText_age.IsEnabled = false;
                    Employee_Btn_SaveAll.IsEnabled = false;
                    Employee_tbox_UserID.Text = empe.user_id.ToString();
                    Employee_tbox_UserID.IsEnabled = false;
                    foreach (Department item in Employee_cbbox_department.Items)
                    {
                        if (item.id == empe.department_id)
                            Employee_cbbox_department.SelectedItem = item;
                        break;
                    }
                    foreach (Job item in Employee_cbbox_jobId.Items)
                    {
                        if (item.id == empe.job_id)
                            Employee_cbbox_jobId.SelectedItem = item;
                        break;
                    }

                    foreach (var xxx in Employee_cbb_gender.Items)
                    {
                        if (xxx.ToString() == empe.gender)
                            Employee_cbb_gender.SelectedItem = xxx;
                        break;
                    }

                }
                if (ft.Equals(FormType.View))
                {

                }
            }
            catch (Exception)
            { 
            }

            
            
        }


        protected async void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int userID, jobID, departmentID, rfid;
                userID = jobID = departmentID = rfid = 1;
                string name, gender, phone, desc;

                userID = Convert.ToInt32(Employee_tbox_UserID.Text);
                jobID = Convert.ToInt32(Employee_cbbox_jobId.SelectedItem.ToString());
                departmentID = Convert.ToInt32(Employee_cbbox_department.SelectedItem.ToString());
                name = Employee_tbox_name.Text;
                gender = Employee_cbb_gender.SelectedItem.ToString();
                phone = Employee_tbox_phone.Text;
                rfid = Convert.ToInt32(Employee_tbox_rfidCard.Text);
                desc = Employee_tbox_description.Text;



                //  name_TextBox

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                EmployeeCreate uc = new EmployeeCreate(userID, name, gender, phone, rfid, desc, jobID, departmentID);
                await vc.PostAsync<EmployeeCreate>(uc);

                this.NavigationService.Navigate(new Employees());

            }
            catch (Exception)
            { 
            }
            
        }

        private void Employee_Btn_Back_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }

        private void Employee_Btn_update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int phoneNo;
                try
                {
                    phoneNo = Convert.ToInt32(Employee_tbox_phone.Text.ToString());
                    if (phoneNo == null || phoneNo > 99999999999 || phoneNo < 100000000)
                    {
                        // Please select group id
                        MessageBox.Show("Please check phone number 'phoneNo > 99999999999 || phoneNo < 100000000' !");
                        return;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Please check phone number 'phoneNo > 99999999999 || phoneNo < 100000000' !");
                    return;
                }

                try
                {
                    empe.gender = Employee_cbb_gender.SelectedItem.ToString();
                    empe.phone = phoneNo + "";
                    var de = (Department)Employee_cbbox_department.SelectedItem;
                    empe.department_id = de.id;
                    var jo = (Job)Employee_cbbox_jobId.SelectedItem;
                    empe.job_id = jo.id;
                    empe.description = Employee_tbox_description.Text;
                    empe.rfidcard_id = Convert.ToInt32(Employee_tbox_rfidCard.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Input Error !\n" + ex);
                }

                try
                {
                    VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                    vc.UpdateAsync<Employee>(empe, empe.id);
                    MessageBox.Show("Employee is updated !");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Employee is not updated !\n" + ex);
                }
            }
            catch (Exception)
            { 
            }
            
           
        }
         

    }


}
