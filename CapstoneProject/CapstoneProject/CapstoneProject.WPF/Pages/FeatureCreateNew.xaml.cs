﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for FeatureCreateNew.xaml
    /// </summary>
    public partial class FeatureCreateNew : Page
    {
        public FeatureCreateNew()
        {
            InitializeComponent();
           
        }



        private void Feature_Btn_SaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name, gender, desc;
                name = gender = desc = "xxx";

                name = Feature_tbox_namevn.Text;
                gender = Feature_tbox_nameen.Text;
                desc = Feature_tbox_desc.Text;

                //  name_TextBox

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                FeatureCreate uc = new FeatureCreate(name, gender, desc);
                vc.PostAsync<FeatureCreate>(uc);
            }
            catch (Exception)
            { 
            }
            
        }

        private void Employee_Btn_Back_Click(object sender, RoutedEventArgs e)
        { 
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }

        private void Employee_Btn_Update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name, gender, desc;
                name = gender = desc = "xxx";

                name = Feature_tbox_namevn.Text;
                gender = Feature_tbox_nameen.Text;
                desc = Feature_tbox_desc.Text;

                //  name_TextBox

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                FeatureCreate uc = new FeatureCreate(name, gender, desc);
                vc.PostAsync<FeatureCreate>(uc);
            }
            catch (Exception)
            {
            }
        }
    }
}
