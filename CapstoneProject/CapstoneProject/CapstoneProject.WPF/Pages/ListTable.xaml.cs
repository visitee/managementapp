﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Syncfusion.UI.Xaml.Grid;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ListTable.xaml
    /// </summary>
    public partial class ListTable : Page
    {
        #region variable
        private Dictionary<Type, ObservableCollection<MappingName>> tableColumnMapping = new Dictionary<Type, ObservableCollection<MappingName>>();
        private ObservableCollection<ListMappingName> listTable = new ObservableCollection<ListMappingName>();
        private Type currentType;
        private RfidCard currentRfidcard = new RfidCard();
        #endregion

        #region contructor
        public ListTable()
        {
            InitializeComponent();
            initTableColumn();
            initListTableCombobox();
            addListComboItem();
            var viewModel = this.DataContext as EntityInfoViewModel;
            viewModel.filterChanged = OnFilterChanged;
        }
        #endregion

        #region don't need to care
        private void addListComboItem()
        {
            ListCombo.ItemTemplate = (DataTemplate)Resources["comboboxitemstyle"];
            foreach (var item in listTable)
            {
                ListCombo.Items.Add(item);
            }
        }

        private void addColumnComboItem(Type o)
        {
            var list = tableColumnMapping[o];
            columnCombo.ItemTemplate = (DataTemplate)Resources["comboboxitemstyle"];
            columnCombo.Items.Clear();
            sfGrid.Columns.Clear();
            columnCombo.Items.Add(new MappingName() { Header = Properties.Resources.MappingName_AllColumn, Name = "All Columns" });
            foreach (var mapping in list)
            {
                columnCombo.Items.Add(mapping);
                GridTextColumn column = new GridTextColumn();
                column.HeaderText = mapping.Header;
                column.MappingName = mapping.Name;
                sfGrid.Columns.Add(column);
            }
            
        }

        private void OnFilterChanged()
        {
            var viewModel = this.DataContext as EntityInfoViewModel;
            if (sfGrid.View != null)
            {
                sfGrid.View.Filter = viewModel.FilerRecords;
                sfGrid.View.RefreshFilter();
            }
        }
        #endregion

        //Hàm để  bắt sự kiện thay đổi danh mục
        private async void ListCombo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            try
            {
                ListMappingName tabletype = (ListMappingName)e.AddedItems[0];
                Type x = tabletype.type;
                currentType = x;
                addColumnComboItem(x);
                object t = new object();
                if (x.Equals(typeof(Employee)))
                {
                    t = await vc.GetAsync<List<Employee>>("employees");
                }
                else if (x.Equals(typeof(Service)))
                {
                    t = await vc.GetAsync<List<Service>>("services");
                }
                else if (x.Equals(typeof(Booking)))
                {
                    t = await vc.GetAsync<List<Booking>>("bookings");
                }
                else if (x.Equals(typeof(RfidCard)))
                {
                    t = await vc.GetAsync<List<RfidCard>>("rfidcards");
                    var listroom = await vc.GetAsync<List<Room>>("rooms");
                    List<RfidCard> list = (List<RfidCard>)t;
                    foreach(var card in list)
                    {
                        foreach(var room in listroom)
                        {
                            if (room.id.Equals(card.room_id))
                                card.room = room.name;
                        }
                    }
                }
                else if (x.Equals(typeof(Billing)))
                {
                    t = await vc.GetAsync<List<Billing>>("billings");
                }
                else if (x.Equals(typeof(Guest)))
                {
                    t = await vc.GetAsync<List<Guest>>("guests");
                }
                else if (x.Equals(typeof(Room)))
                {
                    t = await vc.GetAsync<List<Room>>("rooms");
                }
                else if (x.Equals(typeof(ServiceOrder)))
                {
                    t = await vc.GetAsync<List<ServiceOrder>>("serviceorders");
                }
                else if (x.Equals(typeof(Floor)))
                {
                    t = await vc.GetAsync<List<Floor>>("floors");
                }
                else if (x.Equals(typeof(Department)))
                {
                    t = await vc.GetAsync<List<Department>>("departments");
                }
                else if (x.Equals(typeof(Job)))
                {
                    t = await vc.GetAsync<List<Job>>("jobs");
                }
               

                sfGrid.ItemsSource = t;
            }
            catch (Exception ex)
            {
                var x = ex;
            }
            sfGrid.View.Refresh();
        }

        //Hàm để bắt sự kiện select ở bảng
        private void sfGrid_SelectionChanged(object sender, Syncfusion.UI.Xaml.Grid.GridSelectionChangedEventArgs e)
        {
            Btn_Edit.IsEnabled = true;
            Btn_Detail.IsEnabled = true;
            Btn_Delete.IsEnabled = true;
            var a = (GridRowInfo)e.AddedItems[0];
            if (a.RowData.GetType().Equals(typeof(Employee))) {
                Employee em = (Employee)a.RowData;
            }
            else if (a.RowData.GetType().Equals(typeof(Service)))
            {
                Service service = (Service)a.RowData;
            }
            else if (a.RowData.GetType().Equals(typeof(RfidCard)))
            {
                currentRfidcard = (RfidCard)a.RowData;
            }
            else if (a.RowData.GetType().Equals(typeof(Booking)))
            {
                Booking booking = (Booking)a.RowData;
            }

            else if (a.RowData.GetType().Equals(typeof(Billing)))
            {
                Billing billing = (Billing)a.RowData;
            }
             else if (a.RowData.GetType().Equals(typeof(Guest)))
            {
                Guest guest = (Guest)a.RowData;
            }

            else if (a.RowData.GetType().Equals(typeof(Room)))
            {
                Room room = (Room)a.RowData;
            }

            else if (a.RowData.GetType().Equals(typeof(ServiceOrder)))
            {
                ServiceOrder serviceorder = (ServiceOrder)a.RowData;
            }
            else if (a.RowData.GetType().Equals(typeof(Floor)))
            {
                Floor floor = (Floor)a.RowData;
            }
            else if (a.RowData.GetType().Equals(typeof(Department)))
            {
                Department department = (Department)a.RowData;
            }
            else if (a.RowData.GetType().Equals(typeof(Job)))
            {
                Job job = (Job)a.RowData;
            }
            

        }

        //Hàm khởi tạo cột ở bảng
        private void initTableColumn()
        {
            #region xong cai khoi tao cac cot trong bang

            ObservableCollection<MappingName> serviceOrdercollection = new ObservableCollection<MappingName>();
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_ServiceOrder_serviceid, Name = "service_id" });
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_ServiceOrder_quantity, Name = "quantity" });
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_ServiceOrder_description, Name = "description" });
            serviceOrdercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_ServiceOrder_guestid, Name = "guest_id" });
            tableColumnMapping.Add(typeof(ServiceOrder), serviceOrdercollection);

            ObservableCollection<MappingName> floorcollection = new ObservableCollection<MappingName>();
            floorcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            floorcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Floor_name, Name = "name" });
            floorcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Floor_description, Name = "description" });
            tableColumnMapping.Add(typeof(Floor), floorcollection);

            ObservableCollection<MappingName> jobcollection = new ObservableCollection<MappingName>();
            jobcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            jobcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Job_title, Name = "title" });
            jobcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Job_level, Name = "level" });
            jobcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Job_description, Name = "description" });
            tableColumnMapping.Add(typeof(Job), jobcollection);

             
            ObservableCollection<MappingName> billingcollection = new ObservableCollection<MappingName>();
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_valuevnd, Name = "valuevnd" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_valueusd, Name = "valueusd" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_status, Name = "status" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_overtime, Name = "overtime" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_overvaluevnd, Name = "overvaluevnd" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_overvalueusd, Name = "overvalueusd" });
            billingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Billing_guestid, Name = "guest_id" });
            tableColumnMapping.Add(typeof(Billing), billingcollection);


            ObservableCollection<MappingName> roomcollection = new ObservableCollection<MappingName>();
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_name, Name = "name" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_no, Name = "no" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_status, Name = "status" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_desc, Name = "description" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_floor, Name = "floor_id" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_type, Name = "roomtype_id" });
            roomcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Room_deviceId, Name = "device_id" });
            tableColumnMapping.Add(typeof(Room), roomcollection);

            ObservableCollection<MappingName> guestcollection = new ObservableCollection<MappingName>();
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_name, Name = "name" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_desc, Name = "description" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_phone, Name = "phone" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_guestType, Name = "guesttype" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_count, Name = "count" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_csID, Name = "cstidno" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_national, Name = "nationality" });
            guestcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Guest_userID, Name = "user_id" });
            tableColumnMapping.Add(typeof(Guest), guestcollection);

            ObservableCollection<MappingName> departmentcollection = new ObservableCollection<MappingName>();
            departmentcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            departmentcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Department_name, Name = "name" });
            departmentcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Department_desc, Name = "description" });
            departmentcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Department_totalE, Name = "totalemployees" });
            tableColumnMapping.Add(typeof(Department), departmentcollection);


            ObservableCollection<MappingName> employeecollection = new ObservableCollection<MappingName>();
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Name, Name = "name" });
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Gender, Name = "gender" });
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Phone, Name = "phone" });
            employeecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Description, Name = "description" });
            tableColumnMapping.Add(typeof(Employee), employeecollection);

            ObservableCollection<MappingName> servicecollection = new ObservableCollection<MappingName>();
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Name, Name = "name" });
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Service_Code, Name = "code" });
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Service_Price, Name = "price" });
            servicecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Description, Name = "description" });
            tableColumnMapping.Add(typeof(Service), servicecollection);

            ObservableCollection<MappingName> rfidcollection = new ObservableCollection<MappingName>();
            rfidcollection.Add(new MappingName { Header = Properties.Resources.MappingName_Id, Name = "id" });
            rfidcollection.Add(new MappingName { Header = Properties.Resources.MappingName_Employee_Name, Name = "name" });
            rfidcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Employee_Description, Name = "description" });
            rfidcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Rfid_Room, Name = "room" });
            tableColumnMapping.Add(typeof(RfidCard), rfidcollection);

            // Hello VN
            

            ObservableCollection<MappingName> bookingcollection = new ObservableCollection<MappingName>();
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_roomID, Name = "room_id" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_guestID, Name = "guest_id" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_status, Name = "status" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_createdAt, Name = "created_at" });
            bookingcollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Booking_updatedAt, Name = "updated_at" });
            tableColumnMapping.Add(typeof(Booking), bookingcollection);

            #endregion xong
            //emp, ser, rf, boki, depar, guest, rôm, billing

        }

        //Thêm các danh mục vào combobox
        private void initListTableCombobox()
        {
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Employee, type = typeof(Employee) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Department, type = typeof(Department) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Booking, type = typeof(Booking) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Guest, type = typeof(Guest) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Service, type = typeof(Service) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Room, type = typeof(Room) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Rfid, type = typeof(RfidCard) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Billing, type = typeof(Billing) });
          //  listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Feature, type = typeof(Feature) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Serviceorder, type = typeof(ServiceOrder) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Floor, type = typeof(Floor) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Department, type = typeof(Department) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Job, type = typeof(Job) });

            //cần thêm danh mục user/usergroup/roomtype/device/ lưu ý là không có feature
            /*  Floor, Department, Job  */
        }

        private async void Btn_Delete_Click(object sender, RoutedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
             
            try
            {
              //  ListMappingName tabletype = (ListMappingName)e.AddedItems[0];
              //  Type x = tabletype.type;
               // currentType = x;
                
                if (currentType.Equals(typeof(Booking)))
                {
                    Booking a = (Booking)sfGrid.SelectedItem; 
                    await vc.DeleteAsync<Booking>(a.id);  

                }
                else if (currentType.Equals(typeof(Billing)))
                {
                    Billing a = (Billing)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Billing>(a.id);

                }
                else if (currentType.Equals(typeof(Department)))
                {
                    Department a = (Department)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Department>(a.id);

                }

                else if (currentType.Equals(typeof(Employee)))
                {
                    Employee a = (Employee)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Employee>(a.id);

                }
                else if (currentType.Equals(typeof(Floor)))
                {
                    Floor a = (Floor)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Floor>(a.id);

                }
                else if (currentType.Equals(typeof(Guest)))
                {
                    Guest a = (Guest)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Guest>(a.id);

                }
                else if (currentType.Equals(typeof(Job)))
                {
                    Job a = (Job)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Job>(a.id);

                }
                else if (currentType.Equals(typeof(Room)))
                {
                    Room a = (Room)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Room>(a.id);

                }
                else if (currentType.Equals(typeof(RoomType)))
                {
                    RoomType a = (RoomType)sfGrid.SelectedItem;
                    await vc.DeleteAsync<RoomType>(a.id);

                }
                else if (currentType.Equals(typeof(ServiceOrder)))
                {
                    ServiceOrder a = (ServiceOrder)sfGrid.SelectedItem;
                    await vc.DeleteAsync<ServiceOrder>(a.id);

                }
                else if (currentType.Equals(typeof(Service)))
                {
                    Service a = (Service)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Service>(a.id);

                } 

            }
            catch (Exception ex)
            {

                MessageBox.Show("Deo biet vi sao loi\n" + ex);
            }

            MessageBox.Show("Delete successfully !!!");


        }

        private void Btn_Edit_Click(object sender, RoutedEventArgs e)
        {
            Session.Parameters["isEdit"] = true;
            if (currentType.Equals(typeof(RfidCard)))
            {
                Session.Parameters["rfidcard"] = sfGrid.SelectedItem; 
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new DetailRfid());
            }  else 
            if (currentType.Equals(typeof(Employee)))
            {
                Session.Parameters["employee"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new Employees());
            }   else
            if (currentType.Equals(typeof(Service)))
            {
                Session.Parameters["service"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new ServiceCreate());
            }
            else
              if (currentType.Equals(typeof(ServiceOrder)))
            {
                Session.Parameters["serviceorder"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new ServiceOrderNew());
            }
            else
            if (currentType.Equals(typeof(Booking)))
            {
                Session.Parameters["booking"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new BookingRoom());
            }
            else
            if (currentType.Equals(typeof(Billing)))
            {
                Session.Parameters["billing"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new BookingRoom());
            }
            else
            if (currentType.Equals(typeof(Department)))
            {
                //  if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                //      ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                //  ((MainWindow)App.Current.MainWindow).frame.Navigate(new DepartmentNew());
            }
            else
            if (currentType.Equals(typeof(Guest)))
            {
                Session.Parameters["guest"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new GuestNew());
            }
            else
            if (currentType.Equals(typeof(Floor)))
            {
                Session.Parameters["floor"] = sfGrid.SelectedItem;
                //   if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                //      ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                //  ((MainWindow)App.Current.MainWindow).frame.Navigate(new FloorNew());
            }
            else
            if (currentType.Equals(typeof(Job)))
            {
                Session.Parameters["job"] = sfGrid.SelectedItem;
                //   if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                //       ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                //   ((MainWindow)App.Current.MainWindow).frame.Navigate(new JobNew());
            }
            else
            if (currentType.Equals(typeof(Room)))
            {
                Session.Parameters["room"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new RoomNew());
            }
            else
            if (currentType.Equals(typeof(Usergroup)))
            {
                Session.Parameters["usergroup"] = sfGrid.SelectedItem;
                //  if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                //      ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                //  ((MainWindow)App.Current.MainWindow).frame.Navigate(new UsergroupNew());
            }
            else
            if (currentType.Equals(typeof(User)))
            {
                Session.Parameters["user"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new UserNew());
            }
            else
            if (currentType.Equals(typeof(Permission)))
            {
                Session.Parameters["permission"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new SetPermition());
            }
            else
            if (currentType.Equals(typeof(Feature)))
            {
                Session.Parameters["feature"] = sfGrid.SelectedItem;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new FeatureCreateNew());
            }
        }

        private void Btn_Add_New_Click(object sender, RoutedEventArgs e)
        {

            Session.Parameters["isEdit"] = false;
            if (currentType.Equals(typeof(Employee)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new Employees());
            }
            if (currentType.Equals(typeof(Service)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new ServiceCreate());
            }

            if (currentType.Equals(typeof(RfidCard)))
            { 
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new DetailRfid());
            }

            if (currentType.Equals(typeof(ServiceOrder)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new ServiceOrderNew());
            }
            if (currentType.Equals(typeof(Booking)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new BookingRoom());
            }
            if (currentType.Equals(typeof(Billing)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new BookingRoom());
            }
            if (currentType.Equals(typeof(Department)))
            {
              //  if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
              //      ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
              //  ((MainWindow)App.Current.MainWindow).frame.Navigate(new DepartmentNew());
            }
            if (currentType.Equals(typeof(Guest)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new GuestNew());
            }
            if (currentType.Equals(typeof(Floor)))
            {
             //   if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
              //      ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
              //  ((MainWindow)App.Current.MainWindow).frame.Navigate(new FloorNew());
            }
            if (currentType.Equals(typeof(Job)))
            {
             //   if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
             //       ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
             //   ((MainWindow)App.Current.MainWindow).frame.Navigate(new JobNew());
            }
            if (currentType.Equals(typeof(Room)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new RoomNew());
            }
            if (currentType.Equals(typeof(Usergroup)))
            {
              //  if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
              //      ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
              //  ((MainWindow)App.Current.MainWindow).frame.Navigate(new UsergroupNew());
            }
            if (currentType.Equals(typeof(User)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new UserNew());
            }
            if (currentType.Equals(typeof(Permission)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new SetPermition());
            }
            if (currentType.Equals(typeof(Feature)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new FeatureCreateNew());
            }
        }

        private void Btn_Detail_Click(object sender, RoutedEventArgs e)
        {
            if (currentType.Equals(typeof(RfidCard)))
            {
                Session.Parameters["rfid"] = currentRfidcard;
                Session.Parameters["detail"] = true;
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new DetailRfid());
            }
        }
    }

    #region don't need to care
    public class MappingName
    {
        public string Header { get; set; }
        public string Name { get; set; }
    }

    public class ListMappingName
    {
        public string Header { get; set; }
        public Type type { get; set; }
    }
    #endregion
}
