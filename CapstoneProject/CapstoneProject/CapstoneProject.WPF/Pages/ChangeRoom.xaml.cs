﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ChangeRoom.xaml
    /// </summary>
    public partial class ChangeRoom : Page
    {
        List<Room> usingRoom = new List<Room>();
        List<Room> changingToRoom = new List<Room>();

        Room isUsedRoom = new Room();
        Room willBeUsedRoom = new Room();

        List<Booking> bookingInfoRoom = new List<Booking>();
        Booking bookingisUsed = new Booking();
        int guestID = 0;
        // int billID = 0;

        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);

        public ChangeRoom()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
            }
            this.Loaded += AddNewUserLoaded;
        }


        //private FormType 
        FormType ft = FormType.Add;

        private void loaddatatotable()
        {
            this.Dispatcher.Invoke((Action)(async () =>
            {
                usingRoom = await vc.GetSqlAsync<List<Room>>("Select * from rooms where status=2");
                changeRoom_cbbox_FromRoom.ItemsSource = usingRoom;

                changingToRoom = await vc.GetSqlAsync<List<Room>>("Select * from rooms where status=1");
                ChangeRoom_cbbox_toRoom.ItemsSource = changingToRoom;
            }));

        }

        protected async void AddNewUserLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                changeRoom_btn_changeRoom.IsEnabled = false;
                changeRoom_btn_stopUsing.IsEnabled = false;

                if (ft.Equals(FormType.Add))
                {
                    Thread loadDataToTable = new Thread(loaddatatotable);
                    loadDataToTable.Start();

                }
                else if (ft.Equals(FormType.Edit))
                {

                }
                else if (ft.Equals(FormType.View))
                {

                }
            }
            catch (Exception)
            { 
            }
            

        }


        private async void changeRoom_cbbox_FromRoom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            try
            {
                changeRoom_btn_changeRoom.IsEnabled = true;
                changeRoom_btn_stopUsing.IsEnabled = true;

                var a = (ComboBox)sender;
                var b = (Room)a.SelectedItem;
                isUsedRoom = b;

                int booking_status = 2; // chua thanh toan
                string sql = "Select * from bookings where room_id = " + isUsedRoom.id + " and status = " + booking_status;
                bookingInfoRoom = await vc.GetSqlAsync<List<Booking>>(sql);
                guestID = bookingInfoRoom[0].guest_id;
                bookingisUsed = bookingInfoRoom[0];
            }
            catch (Exception Ex)
            {
                guestID = 1; 
            }
        }

        private void ChangeRoom_cbbox_toRoom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            changeRoom_btn_changeRoom.IsEnabled = true;
            changeRoom_btn_stopUsing.IsEnabled = false;
            try
            {
                var a = (ComboBox)sender;
                var b = (Room)a.SelectedItem;
                willBeUsedRoom = b;
            }
            catch (Exception)
            { 
            } 
            
        }

        private async void changeRoom_btn_stopUsing_Click(object sender, RoutedEventArgs e)
        {
            try
            { 
                isUsedRoom.status = 1;
                vc.UpdateAsync<Room>(isUsedRoom, isUsedRoom.id); 
                vc.UpdateAsync<Booking>(bookingisUsed, bookingisUsed.id);
                MessageBox.Show("Stop using Room is finished successfully !");
                
                changeRoom_btn_changeRoom.IsEnabled = false;
                changeRoom_btn_stopUsing.IsEnabled = false;
                return;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Stop using Room is not finished !");
                return;
            }

        }


        private async void changeRoom_btn_changeRoom_Click(object sender, RoutedEventArgs e)
        {
            var selectedUserGroup = ChangeRoom_cbbox_toRoom.SelectedValue;
            if (selectedUserGroup == null)
            { 
                MessageBox.Show("Please select toRoom @!@");
                return;
            }
            var xxx = changeRoom_cbbox_FromRoom.SelectedValue;
            if (xxx == null)
            {
                MessageBox.Show("Please select fromRoom @!@");
                return;
            }

            try
            {  //update Room tra ve trang thai Fee
                isUsedRoom.status = 1;
                vc.UpdateAsync<Room>(isUsedRoom, isUsedRoom.id);


                vc.UpdateAsync<Booking>(bookingisUsed, bookingisUsed.id);

                //  create Booking theo Guest_ID, Room_ID moi'
                int booking_status = 2; // chua thanh toan
                BookingCreate bbc = new BookingCreate(booking_status, willBeUsedRoom.id, guestID);
                var bookingNew = JsonConvert.DeserializeObject<Booking>(await vc.PostAsync<BookingCreate>(bbc));

                // update Room moi se dc dung
                willBeUsedRoom.status = 2;
                vc.UpdateAsync<Room>(willBeUsedRoom, willBeUsedRoom.id);
                changeRoom_btn_changeRoom.IsEnabled = false;
                changeRoom_btn_stopUsing.IsEnabled = false;
                MessageBox.Show("Change Room is finished successfully !");
                return;

            }
            catch (Exception ex)
            {

                MessageBox.Show("Change Room is not finished !");
                return;
            }


        }

        private async void changeRoom_btn_back_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }
    }
}
