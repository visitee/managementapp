﻿ 
using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CapstoneProject.WPF.Pages; 

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ListService.xaml
    /// </summary>
    public partial class ListService : Page
    {
        public ListService()
        {
            InitializeComponent();
           this.Loaded += ListRoomInfo_Loaded;
        }

        private async void ListRoomInfo_Loaded(object sender, RoutedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            try
            {
                var listem = await vc.GetAsync<List<Service>>("services");
                sfGrid_ListService.ItemsSource = listem;
            }
            catch { vc.cancelPendingRequests(); }

            var viewModel = this.DataContext as EntityInfoViewModel;
            viewModel.filterChanged = OnFilterChanged;


        }

        private void OnFilterChanged()
        {
            var viewModel = this.DataContext as EntityInfoViewModel;
            if (sfGrid_ListService.View != null)
            {
                sfGrid_ListService.View.Filter = viewModel.FilerRecords;
                sfGrid_ListService.View.RefreshFilter();
            }
        }

        private void sfGrid_ListService_SelectionChanged(object sender, Syncfusion.UI.Xaml.Grid.GridSelectionChangedEventArgs e)
        {
            var a = sfGrid_ListService;
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            // go to Create new Service
            this.NavigationService.Navigate(new ServiceCreate());
        }

        private void Service_btn_Order_Click(object sender, RoutedEventArgs e)
        {
            // go to Create new Service
            this.NavigationService.Navigate(new ServiceOrderNew());
        }
    }
}
