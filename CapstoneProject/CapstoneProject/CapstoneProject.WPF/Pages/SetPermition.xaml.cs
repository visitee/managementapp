﻿
using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for SetPermition.xaml
    /// </summary>
    public partial class SetPermition : Page
    {
        List<Usergroup> UsergroupList;
        List<Feature> FeatureList;
        List<Permission> PermissionList;
        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        Permission perMission;


        public SetPermition()
        {
            InitializeComponent();
            this.Loaded += AddNewUserLoaded;
        }

        private void loaddatatotable()
        {
            this.Dispatcher.Invoke((Action)(async () =>
            {
                UsergroupList = await vc.GetAsync<List<Usergroup>>("usergroups");
                FeatureList = await vc.GetAsync<List<Feature>>("features");
                PermissionList = await vc.GetAsync<List<Permission>>("permissions");

                Usergroup_cbbox_usergroup.ItemsSource = UsergroupList; 

            }));

        }

        protected void AddNewUserLoaded(object sender, RoutedEventArgs e)
        {
            try
            { 
                Thread loadDataToTable = new Thread(loaddatatotable);
                loadDataToTable.Start();
            }
            catch (Exception)
            { 
            }
              
        }
        
          
        private async void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                var a = (CheckBox)sender;
                var c = (Feature)a.DataContext;
                var b = (Usergroup)Usergroup_cbbox_usergroup.SelectedItem;

                PermissionCreate uc = new PermissionCreate(b.id, c.id, "None");
                var xxx = await vc.PostAsync<PermissionCreate>(uc); 
            }
            catch (Exception)
            {
            }

        }

        private void Usergroup_cbbox_usergroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                for (int i = 0; i < FeatureList.Count; i++)
                {
                    FeatureList[i].isChecked = false;
                }
                var a = (ComboBox)sender;
                var b = (Usergroup)a.SelectedItem;
                int x = b.id;


                for (int j = 0; j < PermissionList.Count; j++)
                {
                    if (x == PermissionList[j].usergroup_id)
                    {
                        for (int i = 0; i < FeatureList.Count; i++)
                        {
                            if (FeatureList[i].id == PermissionList[j].feature_id) FeatureList[i].isChecked = true;
                        }
                    }
                }
                Usergroup_cbbox_permission.ItemsSource = new List<Feature>();
                Usergroup_cbbox_permission.ItemsSource = FeatureList;
            }
            catch (Exception)
            { 
            }
            
        }

        private void Permit_cbb_check_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                var a = (CheckBox)sender;
                var c = (Feature)a.DataContext;
                var b = (Usergroup)Usergroup_cbbox_usergroup.SelectedItem;
                var x = e;


                for (int j = 0; j < PermissionList.Count; j++)
                {
                    if (b.id == PermissionList[j].usergroup_id & c.id == PermissionList[j].feature_id)
                    {
                        vc.DeleteAsync<Permission>(PermissionList[j].id);
                    }
                }
            }
            catch (Exception)
            { 
            }
           

        }
           
        private void UsergroupNew_Btn_backAll_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }
    }
}
