﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for UserNew.xaml
    /// </summary>
    public partial class UserNew : Page
    {
        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);

        User updateUser;
        public UserNew()
        { 
                InitializeComponent();
            

            try
            { 
                if( (bool)Session.Parameters["isEdit"]) {
                    var user = Session.Parameters["user"];
                    updateUser = (User)user;
                    //code xu ly
                    Session.Parameters.Remove("user");
                    this.Loaded += updateUserLoaded;
                }
                else this.Loaded += AddNewUserLoaded;
            }
            catch
            {
                this.Loaded += AddNewUserLoaded;
            }
            
        }


        //private FormType 
        FormType ft = FormType.Add;

        protected async void updateUserLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Usergroup> roomtypeList = await vc.GetAsync<List<Usergroup>>("usergroups");
                UserNew_cbbox_groupID.ItemsSource = roomtypeList;

                UserNew_tbox_username.Text = updateUser.username;
                UserNew_tbox_username.IsEnabled = false;
                Employee_Btn_SaveAll.IsEnabled = false;
                foreach (Usergroup item in UserNew_cbbox_groupID.Items)
                {
                    if (item.id == updateUser.usergroup_id)
                        UserNew_cbbox_groupID.SelectedItem = item;
                    break;
                }
            }
            catch (Exception)
            { 
            }
             
        }

        protected async void AddNewUserLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                List<Usergroup> roomtypeList = await vc.GetAsync<List<Usergroup>>("usergroups");

                UserNew_cbbox_groupID.ItemsSource = roomtypeList;

                Employee_Btn_Update.IsEnabled = false;
            }
            catch (Exception)
            { 
            }
            
        }

        private async void UserNew_Btn_SaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //System.Console.WriteLine("xxx" + Booking_ccbox_guestID.Text);
                int group_id;
                string pass, name, cf_pass;
                var selectedUserGroup = UserNew_cbbox_groupID.SelectedValue;
                if (selectedUserGroup == null)
                {
                    // Please select group id
                    MessageBox.Show("Please select user group.");
                    return;
                }
                group_id = (selectedUserGroup as Usergroup).id;

                name = UserNew_tbox_username.Text;
                pass = UserNew_tbox_pw.Password;
                cf_pass = UserNew_tbox_cf_Pw.Password;

                if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(pass) || string.IsNullOrWhiteSpace(cf_pass))
                {
                    MessageBox.Show("Please fill in all the text fields.");
                    return;
                }

                if (!string.Equals(pass, cf_pass))
                {
                    MessageBox.Show("Password and confirmation password is not matched.");
                    return;
                }

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                UserCreate uc = new UserCreate(name, pass, cf_pass, group_id);
                var createNewUserRequest = string.Empty;

                try
                {
                    createNewUserRequest = await vc.PostAsync<UserCreate>(uc);
                }
                catch (Exception)
                {
                }

                try
                {
                    User user = Newtonsoft.Json.JsonConvert.DeserializeObject<User>(createNewUserRequest);
                    MessageBox.Show("New user is created!");
                }
                catch (Exception)
                {
                    MessageBox.Show("This user has already taken!");
                }
            }
            catch (Exception)
            { 
            }
            
        }
         

        private void Employee_Btn_Back_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }

        private void Employee_Btn_Update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //System.Console.WriteLine("xxx" + Booking_ccbox_guestID.Text);
                int group_id;
                string pass, name, cf_pass;
                var selectedUserGroup = UserNew_cbbox_groupID.SelectedValue;
                if (selectedUserGroup == null)
                {
                    // Please select group id
                    MessageBox.Show("Please select user group.");
                    return;
                }
                group_id = (selectedUserGroup as Usergroup).id;

                name = UserNew_tbox_username.Text;
                pass = UserNew_tbox_pw.Password;
                cf_pass = UserNew_tbox_cf_Pw.Password;

                if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(pass) || string.IsNullOrWhiteSpace(cf_pass))
                {
                    MessageBox.Show("Please fill in all the text fields.");
                    return;
                }

                if (!string.Equals(pass, cf_pass))
                {
                    MessageBox.Show("Password and confirmation password is not matched.");
                    return;
                }

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                // UserCreate uc = new UserCreate(name, pass, cf_pass, group_id);
                var createNewUserRequest = string.Empty;

                try
                {
                    // createNewUserRequest = await vc.PostAsync<UserCreate>(uc);
                    vc.UpdateAsync<User>(updateUser, updateUser.id);

                }
                catch (Exception)
                {
                    MessageBox.Show("Can not executing this action !");
                }

                MessageBox.Show("User is updated!");
                this.Loaded += AddNewUserLoaded;
            }
            catch (Exception)
            { 
            }
            

        }
    }
}
