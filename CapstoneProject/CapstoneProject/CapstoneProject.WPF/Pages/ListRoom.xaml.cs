﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using CapstoneProject.WPF.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ListRoom.xaml
    /// </summary>
    public partial class ListRoom : Page
    {
        public ListRoom()
        {
         
            InitializeComponent();
           
            if (String.IsNullOrEmpty(Session.ActiveSession.CurrentAccessTokenData.AccessToken))
            {
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new Login());
            }
            
        }

        private bool firstRun = true;

 

        public async void setBinding(Dictionary<string, List<Room>> collection,int count,int usingcount) {
                EmptyRoomTextBox.Text = Properties.Resources.EmptyRoom + count;
                UsingRoomTextBox.Text = Properties.Resources.UsingRoom + usingcount;
            if (firstRun) {
                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                try
                {
                    var floors = await vc.GetAsync<List<Floor>>("floors");
                    if (floors.Count == collection.Count&&firstRun) { 
                        foreach (var vari in collection)
                        {
                            TextBlock floorLabel = new TextBlock();
                            floorLabel.Text = Properties.Resources.FloorLabel + vari.Key;
                            floorLabel.FontSize = 20;
                            floorLabel.FontWeight = FontWeights.Bold;
                            floorLabel.Margin = new Thickness(40, 0, 0, 0);
                            stackpanel.Children.Add(floorLabel);

                            Line line = new Line();
                            line.X1 = 0;
                            line.X2 = WindowWidth;
                            line.Y1 = 0;
                            line.Y2 = 0;
                            line.Stroke = new SolidColorBrush(Colors.Black);
                            line.StrokeThickness = 1;
                            line.Margin = new Thickness(40, 0, 0, 40);
                            stackpanel.Children.Add(line);

                            ListView listview = new ListView();
                            listview.Tag = vari.Key;
                            listview.VerticalContentAlignment = VerticalAlignment.Center;
                            listview.BorderThickness = new Thickness(0);
                            listview.Background = null;//new SolidColorBrush(Color.FromArgb(10, 11, 12, 13));
                            listview.Margin = new Thickness(40, 0, 0, 40);
                            listview.ItemsSource = collection;
                            listview.ItemTemplate = (DataTemplate)Resources["itemstyle"];
                            listview.ItemsPanel = (ItemsPanelTemplate)Resources["itempanel"];
                            if (((MainWindow)App.Current.MainWindow).collection.Count > 0)
                                stackpanel.Children.Add(listview);

                            listview.SelectionChanged += Listview_SelectionChanged;

                        }
                    firstRun = false;
                    }
                }
                catch { vc.cancelPendingRequests(); }
            }
            foreach (var chil in stackpanel.Children)
            {
                if(chil.GetType().Equals(typeof(ListView)))
                ((ListView)chil).ItemsSource = collection[(string)((ListView)chil).Tag];
            }
        }

        private void Listview_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var x = e;
            var y = sender;
        }
        
    }
}
