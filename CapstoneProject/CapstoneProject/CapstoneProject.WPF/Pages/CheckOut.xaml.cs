﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for CheckOut.xaml
    /// </summary>
    public partial class CheckOut : Page
    {
        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        double paymentForBooking  = 10000.0;
        double paymentForService = 10000.0;
        double totalPayment = 10000.0; 
        double usd_rate = 22050.0;
        // Price  1h = 50k, 
        int oneHourPrice = 50000;


        User current;
        List<Room> roomsList = new List<Room>();
        List<Billing> listem = new List<Billing>();
        Room roomSelected = new Room();
        Billing billingCurent = new Billing();
        Booking bookingCurent = new Booking();

        Guest guestNow = new Guest();
        List<Guest> guestsList = new List<Guest>();
        List<Booking> bookingsList = new List<Booking>();

        List<ServiceOrder> serviceorderList = new List<ServiceOrder>();
        List<Service> serviceList = new List<Service>();
        public CheckOut()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
            }
            this.Loaded += AddNewBooking_Loaded;
        }

        // do dlieu vao Room, click checkin -> nwe Guest, new bill, new booking, guestdetails

        private void loaddatatotable()
        {
            this.Dispatcher.Invoke((Action)(async () =>
            {
                try
                {
                    listem = await vc.GetSqlAsync<List<Billing>>("Select * from billings where status=2"); //GetAsync<List<Billing>>("billings");
                    sfGrid.ItemsSource = listem; 
                    roomsList = await vc.GetSqlAsync<List<Room>>("Select * from rooms where status=2 or status=3");
                    CheckOut_cbb_room.ItemsSource = roomsList;
                    if (listem.Count == 0 || roomsList.Count==0 ) throw new Exception(); 
                }
                catch (Exception Ex)
                {
                    MessageBox.Show("No Room for Check Out");
                    return;
                }
                 
            }));

        }

        protected async void AddNewBooking_Loaded(object sender, RoutedEventArgs e)
        {
            current = await vc.GetAsync<User>("users/" + Session.ActiveSession.CurrentAccessTokenData.userId); 
            serviceList = await vc.GetAsync<List<Service>>("services");
            bookingsList = await vc.GetSqlAsync<List<Booking>>("Select * from bookings where room_id=" + roomSelected.id + " and status=" + 2 + " or status=" + 3); // await vc.GetAsync<List<Booking>>("bookings");
            
            try
            {
                Thread loadDataToTable = new Thread(loaddatatotable);
                loadDataToTable.Start();

                CheckOut_cbb_curencyType.Items.Add("VND");
                CheckOut_cbb_curencyType.Items.Add("USD"); 

                CheckOut_tb_staffID.Text = current.username;
                CheckOut_tb_staffID.IsEnabled = false;

            }
            catch (Exception ex) {
               // MessageBox.Show("loadDataToTable Error!!!");
              //  return;
            } 
        }
        
        private async void CheckOut_btn_CheckOut_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var selectedRoomGroup = CheckOut_cbb_room.SelectedValue;
                if (selectedRoomGroup == null)
                {
                    // Please select group id
                    MessageBox.Show("Please select Room to CheckOut !");
                    return;
                }

                // update bill status
                billingCurent.status = 1;
                billingCurent.valuevnd = (float) totalPayment; 
                billingCurent.valueusd = (float) (totalPayment/usd_rate);
                vc.UpdateAsync<Billing>(billingCurent, billingCurent.id);

                // update booking status
                bookingsList = await vc.GetSqlAsync<List<Booking>>("Select * from bookings where guest_id=" + guestNow.id);
                for (int i = 0; i < bookingsList.Count; i++)
                {
                    bookingsList[i].status = 1;
                    vc.UpdateAsync<Booking>(bookingsList[i], bookingsList[i].id);

                }
                // update room status 
                for (int j = 0; j < bookingsList.Count; j++)
                {
                    for (int i = 0; i < roomsList.Count; i++)
                    {
                        if (roomsList[i].id == bookingsList[j].room_id)
                            roomsList[i].status = 1;
                        vc.UpdateAsync<Room>(roomsList[i], roomsList[i].id);

                    }
                }
                roomsList = await vc.GetSqlAsync<List<Room>>("Select * from rooms where status=2 or status= 3"); // await vc.GetAsync<List<Booking>>("bookings");
                CheckOut_cbb_room.ItemsSource = new List<Room>();
                CheckOut_cbb_room.ItemsSource = roomsList;

                MessageBox.Show("Payment is finished successfully !");
                return;

            }
            catch (Exception ex)
            { 
                MessageBox.Show("Payment is not finished !");
                return;
            }
            

        } 
        
        private void sfGrid_SelectionChanged(object sender, Syncfusion.UI.Xaml.Grid.GridSelectionChangedEventArgs e)
        {
            var a = sfGrid;
        }

        private void CheckOut_cbb_curencyType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var a = (ComboBox)sender;

            double a1 = paymentForBooking / usd_rate;
            double b1 = paymentForService / usd_rate;
            double c1 = totalPayment / usd_rate;

            //string b = (string) a.SelectedItem;
            if ( CheckOut_cbb_curencyType.SelectedIndex == 1 ) // USD
            { 
                CheckOut_tb_serviceFee.Text = b1.ToString();
                CheckOut_tb_hotelFee.Text = a1.ToString();
                CheckOut_tb_totalFee.Text = c1.ToString();

            }
            else  // vnd
            {
                CheckOut_tb_serviceFee.Text = paymentForService.ToString();
                CheckOut_tb_hotelFee.Text = paymentForBooking.ToString();
                CheckOut_tb_totalFee.Text = totalPayment.ToString();
            }

        } 
        private double bookingPayment(Booking booking, int oneHourPrice)
        {
            double x = 0.0;
            var result = booking.updated_at.Subtract(booking.created_at);
            try
            {
                if (result.TotalMinutes > 0)
                { x = ( result.TotalMinutes / 60 ) * oneHourPrice; } 
            }
            catch (Exception ex)
            {   
                x = ( result.TotalMinutes / 60 ) * oneHourPrice + 1.0;
            } 
            return x;
        } 


        private double servicePayment(int guest_id)
        {
            double x = 0.0;
            paymentForService = 0.0;
            try
            {  
                try
                {
                     
                    if (serviceorderList.Count > 0)
                    {
                        for (int i = 0; i < serviceList.Count; i++)
                        {
                            for (int j = 0; j < serviceorderList.Count; j++)
                            {
                                if (serviceorderList[j].service_id == serviceList[i].id)
                                {
                                    paymentForService += serviceorderList[j].quantity * serviceList[i].price;
                                }
                            }

                        }
                    }
                   // else MessageBox.Show("No ServiceOrder for this GuestID =  " + guestNow.id);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("--->>> " + ex);
                }
                return paymentForService;


            }
            catch (Exception ex)
            {
                MessageBox.Show("--->>> "+ ex);
            }
            return paymentForService;

        } 
        private async void CheckOut_cbb_room_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                int room_id = 0;
                try
                {
                    var a = (ComboBox)sender;
                    roomSelected = (Room)a.SelectedItem;
                }  catch (Exception)  {  } 

                room_id = roomSelected.id;
                // int Guest_id=0;

                int year, day, month, hour, minute;

                paymentForBooking = paymentForService = totalPayment = 10000.0;
                guestsList = await vc.GetAsync<List<Guest>>("guests");

                //Update Booking  co guest_id = guestNow, voi cac Room co status=2, cac room da dung co status=1 thi ko update
                bookingsList = await vc.GetSqlAsync<List<Booking>>("Select * from bookings where room_id=" + room_id + " and status=" + 2);
               
                // Tim Guest hien tai cua Phong dc chon
                for (int j = 0; j < guestsList.Count; j++)
                {
                    for (int i = 0; i < bookingsList.Count; i++)
                    {
                        if (  guestsList[j].id == bookingsList[i].guest_id )
                            {
                                guestNow = guestsList[j];
                                break;
                            }
                    } 
                } 
                bookingsList = await vc.GetSqlAsync<List<Booking>>("Select * from bookings where guest_id=" + guestNow.id);

                CheckOut_tb_cusName.Text = guestNow.name;
                CheckOut_tb_phone.Text = guestNow.phone;
                CheckOut_tb_checkInID.Text = guestNow.id.ToString();

                try
                {
                    serviceorderList = await vc.GetSqlAsync<List<ServiceOrder>>("Select * from serviceorders where guest_id=" + guestNow.id);
                }
                catch (Exception ex)
                {
                   // MessageBox.Show("---->>  "+ex);
                }

                try
                {
                    // cap nhat thoi gian cuoi cung, trc luc checkout
                    for (int i = 0; i < roomsList.Count; i++)
                    {
                        for (int j = 0; i < bookingsList.Count; i++)
                        {
                            if (roomsList[i].id == bookingsList[j].room_id & roomsList[i].status == 2)
                            {
                                vc.UpdateAsync<Booking>(bookingsList[j], bookingsList[j].id);
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                     
                }

                


                bookingsList = await vc.GetSqlAsync<List<Booking>>("Select * from bookings where guest_id=" +  guestNow.id); // await vc.GetAsync<List<Booking>>("bookings");
                 
                //CheckOut_tb_cusName.Text = roomSelected.name;
                CheckOut_date_chekcIN.SetValue(Syncfusion.Windows.Shared.DateTimeEdit.DateTimeProperty, roomSelected.created_at);
                CheckOut_date_chekcOut.SetValue(Syncfusion.Windows.Shared.DateTimeEdit.DateTimeProperty, roomSelected.updated_at);

                for (int i = 0; i < bookingsList.Count; i++)
                {
                        // payment for booking room
                        paymentForBooking += bookingPayment(bookingsList[i], oneHourPrice);
                    
                }
                //paymentForService
                paymentForService = servicePayment(guestNow.id); 
                // total payment
                totalPayment = paymentForBooking + paymentForService;

                CheckOut_tb_hotelFee.Text = (Math.Round(paymentForBooking/100d, 0) * 100).ToString();
                CheckOut_tb_serviceFee.Text = (Math.Round(paymentForService / 100d, 0) * 100).ToString();
                CheckOut_tb_totalFee.Text = (Math.Round(totalPayment / 100d, 0) * 100).ToString();

                for (int i = 0; i < listem.Count; i++)
                {
                    if (listem[i].guest_id == guestNow.id)
                    {
                        listem[i].valuevnd += (float)totalPayment;
                        listem[i].valueusd += (float)(totalPayment / usd_rate);
                        billingCurent = listem[i];
                        break;
                    }
                }
                // update billing status
                vc.UpdateAsync<Billing>(billingCurent, billingCurent.id);


            }
            catch (Exception ex)
            {
               // MessageBox.Show("---->>  " + ex);
            } 
        } 
        private void CheckOut_btn_changeRoom_Click(object sender, RoutedEventArgs e)
        {
            if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
            ((MainWindow)App.Current.MainWindow).frame.Navigate(new ChangeRoom());
        }
    }
}

