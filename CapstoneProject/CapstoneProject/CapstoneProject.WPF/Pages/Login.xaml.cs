﻿using CapstoneProject.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF.Pages
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
        }

        private void OnAuthenticationFinished(AccessTokenData session)
        {
            throw new NotImplementedException();
        }

        private async void Login_Click(object sender, RoutedEventArgs e)
        {
            //username.Text = "admin1";
            //password.Password = "password1";
            username.IsEnabled = false;
            password.IsEnabled = false;
            ((Button)sender).IsEnabled = false;
            string pass = password.Password;
            string usern = username.Text;
            
            try { 
                await Session.ActiveSession.LoginAsync("abc", true, Base64Encode(usern + ":" + pass));
                if (Session.ActiveSession.CurrentAccessTokenData.AccessToken == null)
                    throw new Exception("unauthozation");
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new ListRoom());
            }
            catch
            {
                ErrMessage.Text = Properties.Resources.Login_Fail_Message;
                await Task.Delay(3000);
                ErrMessage.Text = "";
            }
            ((Button)sender).IsEnabled = true;
            username.IsEnabled = true;
            password.IsEnabled = true;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
    }
}
