﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ListBills.xaml
    /// </summary>
    public partial class ListBills : Page
    {
        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        List<Billing> ListBillings;
        public ListBills()
        {
            InitializeComponent();
           this.Loaded += ListBill_Loaded;
        }

        private async void loaddatatotable()
        {
            this.Dispatcher.Invoke((Action)(async () =>
            {
                // load billing da dc thanh toan
                ListBillings = await vc.GetSqlAsync<List<Billing>>("Select * from billings where status=1");   
                sfGrid_ListBill.ItemsSource = ListBillings;
            }));
        }

        private async void ListBill_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Thread loadDataToTable = new Thread(loaddatatotable);
                loadDataToTable.Start();
            }
            catch { vc.cancelPendingRequests(); }

            var viewModel = this.DataContext as EntityInfoViewModel;
            viewModel.filterChanged = OnFilterChanged;


        }

        private void OnFilterChanged()
        {
            var viewModel = this.DataContext as EntityInfoViewModel;
            if (sfGrid_ListBill.View != null)
            {
                sfGrid_ListBill.View.Filter = viewModel.FilerRecords;
                sfGrid_ListBill.View.RefreshFilter();
            }
        }

        private void sfGrid_ListBill_SelectionChanged(object sender, Syncfusion.UI.Xaml.Grid.GridSelectionChangedEventArgs e)
        {
            var a = sfGrid_ListBill;
        }

        private void GoToCheckOut_Click(object sender, RoutedEventArgs e)
        {
            // go to Create new ListBills
            this.NavigationService.Navigate(new CheckOut());
        }
         
        
    }


    


}
