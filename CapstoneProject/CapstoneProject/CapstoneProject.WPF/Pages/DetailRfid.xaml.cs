﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Sockets.Plugin;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for DetailRfid.xaml
    /// </summary>
    public partial class DetailRfid : Page
    {
        public DetailRfid()
        {
            InitializeComponent();
            this.Loaded += DetailRfid_Loaded;
        }

        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        List<Room> listroom = new List<Room>();
        List<RfidCard> rfid = new List<RfidCard>();
        Room current = new Room();
        RfidCard currentcard = new RfidCard();
        private async void DetailRfid_Loaded(object sender, RoutedEventArgs e)
        {

            if (formtype.Equals(FormType.Edit))
            {
                try
                { 
                    listroom =await vc.GetAsync<List<Room>>("rooms");
                    rfid = await vc.GetAsync<List<RfidCard>>("rfidcards");
                    ListRoomCombobox.ItemsSource = listroom;
                    //ListRfidCombobox.ItemsSource = rfid;
                    try { 
                        foreach(var item in ListRoomCombobox.Items)
                        {
                            if (((Room)item).id.Equals(((RfidCard)Session.Parameters["rfid"]).room_id)){
                                ListRoomCombobox.SelectedItem = item;
                                break;
                            }
                        }
                        ListRoomCombobox.IsEnabled = false;
                        ListRfidCombobox.IsEnabled = false;
                    }
                    catch { }
                    try {
                        var a = Session.Parameters["detail"];
                        Btn_Update.Visibility = Visibility.Collapsed;
                        Btn_Add.Visibility = Visibility.Collapsed;
                        Session.Parameters.Remove("detail");
                    }
                    catch { }
                    try
                    {
                        var a = Session.Parameters["addnew"];
                        ListRfidCombobox.IsEnabled = false;
                        Btn_Update.Visibility = Visibility.Collapsed;
                        Session.Parameters.Remove("addnew");
                    }
                    catch { }
                }
                catch { }
            }
        }

        FormType formtype=FormType.Edit;

        private void ListRoomCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;

            current = (Room)e.AddedItems[0];
            ObservableCollection<RfidCard> listrfid = new ObservableCollection<RfidCard>();
                foreach(var card in rfid)
                {
                    if (card.room_id.Equals(current.id))
                    {
                        listrfid.Add(card);
                    }
                }
            ListRfidCombobox.ItemsSource = listrfid;
            try { 
                foreach (var item in ListRfidCombobox.Items)
                {
                    if (((RfidCard)item).id.Equals(((RfidCard)Session.Parameters["rfid"]).id)) { 
                        ListRfidCombobox.SelectedItem = item;
                        break;
                    }
                }
                Session.Parameters.Remove("rfid");
            }
            catch { }
        }

        private async void update_Click(object sender, RoutedEventArgs e)
        {
            string ipaddress = "";
            try {
                var listdevice = await vc.GetAsync<List<ArduinoDevice>>("devices");
                foreach(var device in listdevice)
                {
                    if (device.id.Equals(current.device_id))
                        ipaddress = device.ipaddress;
                }
                TcpSocketClient client = new TcpSocketClient();
                await client.ConnectAsync(ipaddress, 23);
                foreach (char a in (current.no+"%"+currentcard.name+"#").ToCharArray())
                    client.WriteStream.WriteByte(Convert.ToByte(a));
                await client.WriteStream.FlushAsync();
                await client.DisconnectAsync();
            }
            
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ListRfidCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0)
                return;
            currentcard = (RfidCard)e.AddedItems[0];
        }

        private async void Btn_Add_Click(object sender, RoutedEventArgs e)
        {
            string ipaddress = "";
            try
            {
                var listdevice = await vc.GetAsync<List<ArduinoDevice>>("devices");
                foreach (var device in listdevice)
                {
                    if (device.id.Equals(current.device_id))
                        ipaddress = device.ipaddress;
                }
                TcpSocketClient client = new TcpSocketClient();
                await client.ConnectAsync(ipaddress, 23);
                foreach (char a in (current.no+"+").ToCharArray())
                    client.WriteStream.WriteByte(Convert.ToByte(a));
                await client.WriteStream.FlushAsync();
                await client.DisconnectAsync();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_Back_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }
    }
}
