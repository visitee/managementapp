﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Syncfusion.UI.Xaml.Charts;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF.Pages
{
    /// <summary>
    /// Interaction logic for FinancialReport.xaml
    /// </summary>
    public partial class FinancialReport : Page
    {
        public FinancialReport()
        {
            InitializeComponent();
            this.Loaded += FinancialReport_Loaded;
            reporttype_cbbox.Items.Add(Properties.Resources.Day);
            reporttype_cbbox.Items.Add(Properties.Resources.Month);
            currencytype_cbbox.Items.Add(Properties.Resources.USD);
            currencytype_cbbox.Items.Add(Properties.Resources.VND);
            reporttype_cbbox.SelectedIndex = 0;
            currencytype_cbbox.SelectedIndex = 0;
        }

        private AnalyzeType currentAnalyzeType = AnalyzeType.DayInMonth;
        private CurrencyType currentCurrencyType = CurrencyType.USD;

        private async void FinancialReport_Loaded(object sender, RoutedEventArgs e)
        {
            
           
        }

        private void XySegmentDraggingBase_OnDragDelta(object sender, DragDelta e)
        {
           // var info = e as XySegmentDragEventArgs;
           // if (info == null) return;
           // info.Cancel = info.NewYValue < 1;
        }
        private void XySegmentDraggingBase_OnDragStart(object sender, ChartDragStartEventArgs e)
        {
            e.Cancel = true;
        }

        private void XySegmentDraggingBase_OnSegmentMouseOver(object sender, XySegmentEnterEventArgs e)
        {
            e.CanDrag = true;
        }

        private async void reporttype_cbbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var x = e.AddedItems[0];
            AnalysisViewModel avm = new AnalysisViewModel();
            if (x.Equals(Properties.Resources.Day))
            {
                currentAnalyzeType = AnalyzeType.DayInMonth;
            }
            else if (x.Equals(Properties.Resources.Month))
            {
                currentAnalyzeType = AnalyzeType.MonthInYear;
            }
            await avm.SetData(currentAnalyzeType, currentCurrencyType);
            gridchart.DataContext = avm;
        }

        private async void currencytype_cbbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var x = e.AddedItems[0];
            AnalysisViewModel avm = new AnalysisViewModel();
            if (x.Equals(Properties.Resources.USD))
            {
                currentCurrencyType = CurrencyType.USD;
                numercalaxis.PrefixLabelTemplate = (DataTemplate)gridchart.Resources["PrefixLabelTemplateUSD"];
            }
            else if (x.Equals(Properties.Resources.VND))
            {
                currentCurrencyType = CurrencyType.VND;
                numercalaxis.PrefixLabelTemplate = (DataTemplate)gridchart.Resources["PrefixLabelTemplateVND"];
            }
            await avm.SetData(currentAnalyzeType, currentCurrencyType);
            gridchart.DataContext = avm;

        }
    }

    public enum AnalyzeType
    {
        DayInMonth,MonthInYear,QuaterInYear,Year
    }

    public enum CurrencyType
    {
        USD,VND
    }

    public class AnalysisViewModel
    {
        public ObservableCollection<AnalysisModel> SalesData { get; set; }

        public AnalysisViewModel()
        {
        }
        
        public async Task SetData(AnalyzeType type,CurrencyType currencytype)
        {
             VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
             List<Billing> billinglist = await vc.GetSqlAsync<List<Billing>>("select * from billings where status = 1 order by created_at");
            ObservableCollection<AnalysisModel> datas = new ObservableCollection<AnalysisModel>();
            string current = "";
            foreach(var bill in billinglist)
            {
                float value = 0;
                if (currencytype.Equals(CurrencyType.USD))
                    value = bill.valueusd;
                else if (currencytype.Equals(CurrencyType.VND))
                    value = bill.valuevnd;

                if (type.Equals(AnalyzeType.DayInMonth))
                {
                    if (current.Equals(bill.created_at.Date.ToShortDateString()))
                    {
                        if (!current.Equals(""))
                        {
                            datas.Last<AnalysisModel>().NoOfCustomer++;
                            datas.Last<AnalysisModel>().Income += value;
                        }
                    }
                    else
                    {
                        datas.Add(new AnalysisModel(bill.created_at.Date.ToShortDateString(), 1, value, ""));
                        current = bill.created_at.Date.ToShortDateString();
                    }
                }
                else if (type.Equals(AnalyzeType.MonthInYear))
                {
                    if (current.Equals(bill.created_at.Month.ToString()))
                    {
                        datas.Last<AnalysisModel>().NoOfCustomer++;
                        datas.Last<AnalysisModel>().Income += value;
                    }
                    else
                    {
                        datas.Add(new AnalysisModel(bill.created_at.Month.ToString(), 1, value, ""));
                        current = bill.created_at.Month.ToString();
                    }
                }
                else if (type.Equals(AnalyzeType.QuaterInYear))
                {
                    if (current.Equals(bill.created_at.Month.ToString()))
                    {
                        datas.Last<AnalysisModel>().NoOfCustomer++;
                        datas.Last<AnalysisModel>().Income += value;
                    }
                    else
                    {
                        datas.Add(new AnalysisModel(bill.created_at.Month.ToString(), 1, value, ""));
                        current = bill.created_at.Month.ToString();
                    }
                }
                   
            }
            SalesData = datas;
        }
    }

    public class AnalysisModel : INotifyPropertyChanged
    {

        private string text;

        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        private double income;

        public double Income
        {
            get { return income; }
            set { income = value; }
        }

        private double noOfCustomer;

        public double NoOfCustomer
        {
            get { return noOfCustomer; }
            set { noOfCustomer = value; }
        }

        private string time;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Time
        {
            get { return time; }
            set { time = value; }
        }

        public AnalysisModel(string year, double count, double income, string text)
        {
            Time = year;
            NoOfCustomer = count;
            Income = income;
            Text = text;
        }
    }
}
