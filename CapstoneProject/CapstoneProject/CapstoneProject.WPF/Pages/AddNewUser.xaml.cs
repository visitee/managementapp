﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for AddNewUser.xaml
    /// </summary>
    /// 
    enum FormType
    {
        Add,
        Edit,
        View,
        Delete
    };
    public partial class AddNewUser : Page
    {
        public AddNewUser()
        {
            try

            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                // Log error (including InnerExceptions!)
                // Handle exception
                System.Console.WriteLine(ex.ToString());
            }
            this.Loaded += AddNewUser_Loaded;
        }

        private FormType ft = FormType.Edit;

        private async void AddNewUser_Loaded(object sender, RoutedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);

            if (ft.Equals(FormType.Add))
            {

            }
            else if (ft.Equals(FormType.Edit))
            {
                combobox.Items.Add("a");
                combobox.Items.Add("b");
                combobox.Items.Add("c");
            }
            else if (ft.Equals(FormType.View))
            {
                List<User> users = await vc.GetAsync<List<User>>("users");

                UserCreate uc = new UserCreate("admin10", "password10", "password10", 1);
                vc.PostAsync<UserCreate>(uc);
                
                User a = users[0];
                
            }
        }

      

        private void combobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var a = e.AddedItems[0];
            System.Console.WriteLine(a);
        }
    }
}
