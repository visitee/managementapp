﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for RoomNew.xaml
    /// </summary>
    public partial class RoomNew : Page
    {
        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
        Room roomNow;
        FormType ft = FormType.Add;
        int flooID, romTye;  

        public RoomNew()
        {

            InitializeComponent();
            try
            {
                if ((bool)Session.Parameters["isEdit"])
                {
                    ft = FormType.Edit;
                    var user = Session.Parameters["room"];
                    roomNow = (Room)user;
                    //code xu ly
                    Session.Parameters.Remove("room");
                    //  this.Loaded += updateLoaded;
                }
                this.Loaded += AddNewRoomLoaded;
            }
            catch
            {
                this.Loaded += AddNewRoomLoaded;
            }

        }


        protected async void AddNewRoomLoaded(object sender, RoutedEventArgs e)
        {
            try
            { 
                List<RoomType> roomtypeList = await vc.GetAsync<List<RoomType>>("roomtypes");
                List<Floor> floorsList = await vc.GetAsync<List<Floor>>("floors");
                RoomNew_cbbox_roomType.ItemsSource = roomtypeList;
                RoomNew_cbbox_floorID.ItemsSource = floorsList;
                if (ft.Equals(FormType.Add))
                { 
                    Employee_Btn_UpdateAll.IsEnabled = false;
                }
                else if (ft.Equals(FormType.Edit))
                {
                    RoomNew_tbox_desc.Text = roomNow.description;
                    RoomNew_tbox_No.Text = roomNow.no;
                    RoomNew_tbox_No.IsEnabled = false;
                    RoomNew_tbox_name.Text = roomNow.name;
                    RoomNew_tbox_status.Text = roomNow.status + "";
                    Employee_Btn_SaveAll.IsEnabled = false;

                    foreach (Floor item in RoomNew_cbbox_floorID.Items)
                    {
                        if (item.id == roomNow.floor_id)
                            RoomNew_cbbox_floorID.SelectedItem = item;
                        break;
                    }
                    foreach (RoomType item in RoomNew_cbbox_roomType.Items)
                    {
                        if (item.id == roomNow.roomtype_id)
                            RoomNew_cbbox_roomType.SelectedItem = item;
                        break;
                    }

                    // roomNow.status = Convert.ToInt32(RoomNew_tbox_status.Text);

                }
                else if (ft.Equals(FormType.View))
                {
                }
            }
            catch (Exception ex)
            {
               // MessageBox.Show("Err: -> " + ex);
               // return;
            }

           
        }

        private async void RoomNew_Btn_SaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //System.Console.WriteLine("xxx" + Booking_ccbox_guestID.Text);
                int status, floor_id, roomtype_id;
                string no, name, description;

                floor_id = Convert.ToInt32(RoomNew_cbbox_floorID.SelectedItem.ToString());
                roomtype_id = Convert.ToInt32(RoomNew_cbbox_roomType.SelectedItem.ToString());
                status = Convert.ToInt32(RoomNew_tbox_status.Text);
                no = RoomNew_tbox_No.Text;
                name = RoomNew_tbox_name.Text;
                description = RoomNew_tbox_desc.Text;
                //  name_TextBox

                VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                RoomCreate uc = new RoomCreate(no, name, status, description, floor_id, roomtype_id);
                await vc.PostAsync<RoomCreate>(uc); 
                MessageBox.Show("Room is created !");
                ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Room is not created !"); 
                return;
            }



        }

        private void Employee_Btn_UpdateAll_Click(object sender, RoutedEventArgs e)
        {
            int status;
            try
            {
                status = Convert.ToInt32(RoomNew_tbox_status.Text); 
                if (status == null || status > 3 || status < 0)
                {
                    // Please select group id
                    MessageBox.Show("Please check status 'status > 0 || status < 3' !");
                    return;
                }
                roomNow.status = status;
            }
            catch (Exception)
            {
                MessageBox.Show("Please check status 'status > 0 || status < 3' !");
                return;
            }
            string name, no;
            try
            {
                name = RoomNew_tbox_name.Text;
                no = RoomNew_tbox_No.Text;
                if (name == null ||  no == null )
                {
                    // Please select group id
                    MessageBox.Show("Please check name or no !");
                    return;
                }
                roomNow.name = name;
                roomNow.no = no;
                roomNow.description = RoomNew_tbox_desc.Text;
            }
            catch (Exception)
            {
                MessageBox.Show("Please check name or no !");
                return;
            }
             
            try
            {
               // VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                vc.UpdateAsync<Room>(roomNow, roomNow.id);
                MessageBox.Show("Room is updated !");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Room is not updated !\n" + ex);
            }
             
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();

        }

        private void Employee_Btn_Back_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }
    }
}



