﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ServiceOrderNew.xaml
    /// </summary>
    public partial class ServiceOrderNew : Page
    {

        Guest guestOrder;
        ServiceOrder serviceOrderNow;
        List<ServiceOrder> serviceOrderList = new List<ServiceOrder>();
        List<Guest> GuestList;
        List<Service> ServiceList;

        VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);

        public ServiceOrderNew()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
            }
            this.Loaded += AddNewUserLoaded;
        }


        private void loaddatatotable()
        {
            this.Dispatcher.Invoke((Action)(async () =>
            {
                GuestList = await vc.GetAsync<List<Guest>>("guests");
                ServiceList = await vc.GetAsync<List<Service>>("services");
                OrderNew_cbbox_GuestName.ItemsSource = GuestList;

                sfGrid.ItemsSource = new List<ServiceOrder>();
                sfGrid.ItemsSource = serviceOrderList;
            }));

        }
         

        FormType ft = FormType.Add;

        protected async void AddNewUserLoaded(object sender, RoutedEventArgs e)
        {
            if (ft.Equals(FormType.Add))
            {
                Thread loadDataToTable = new Thread(loaddatatotable);
                loadDataToTable.Start();
            }
            else if (ft.Equals(FormType.Edit))
            {
            }
            else if (ft.Equals(FormType.View))
            {
            }

        }


        private void OrderNew_cbbox_GuestName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var a = (ComboBox)sender;
                var b = (Guest)a.SelectedItem;

                guestOrder = new Guest();
                guestOrder = b;

                OrderServiceNew_cbbox_ListOrder.ItemsSource = new List<Service>();
                OrderServiceNew_cbbox_ListOrder.ItemsSource = ServiceList;
            }
            catch (Exception)
            {

            }

        }

        private void OrderServiceNew_cbbox_ListOrder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var a = (ComboBox)sender;
                var b = (Service)a.SelectedItem;
                serviceOrderNow = new ServiceOrder();
                serviceOrderNow.guest_id = guestOrder.id;
                serviceOrderNow.service_id = b.id;
            }
            catch (Exception)
            {

            }

        }
         

        private void ServiceOrderNew_Btn_Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                serviceOrderNow.quantity = Convert.ToInt32(OrderNew_tbox_quantity.Text);
                serviceOrderNow.description = OrderNew_box_desc.Text;

                serviceOrderList.Add(serviceOrderNow);

                sfGrid.ItemsSource = new List<ServiceOrder>();
                sfGrid.ItemsSource = serviceOrderList;
            }
            catch (Exception)
            {
            }

        }

        private void Btn_Delete_Click(object sender, RoutedEventArgs e)
        { 
            try
            {
                ServiceOrder a = (ServiceOrder)sfGrid.SelectedItem;
                serviceOrderList.Remove(a);
                sfGrid.ItemsSource = new List<ServiceOrder>();
                sfGrid.ItemsSource = serviceOrderList;

            }
            catch (Exception ex)
            {

               // MessageBox.Show("Can't delete !!!");
            }

           // MessageBox.Show("Delete successfully !!!");


        }

        private void ServiceOrderNew_Btn_GoBack_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();
        }

        private async void ServiceOrderNew_Btn_Update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                for (int i = 0; i < serviceOrderList.Count; i++)
                {
                    ServiceOrderCreate so = new ServiceOrderCreate(serviceOrderList[i].guest_id, serviceOrderList[i].service_id, serviceOrderList[i].quantity, serviceOrderList[i].description);
                    var x = (ServiceOrder)JsonConvert.DeserializeObject<ServiceOrder>(await vc.PostAsync(so));
                    /// serviceOrderList.Add(ServiceOrdList2);
                }
                MessageBox.Show("Service Order is created !");
                ((MainWindow)App.Current.MainWindow).frame.NavigationService.GoBack();

            }
            catch (Exception)
            {
            }
        }
    }
}
