﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for AdminManagementCenter.xaml
    /// </summary>
    public partial class AdminManagementCenter : Page
    {
      //  public AdminManagementCenter()
       // {
          //  InitializeComponent();
        // }
        // }
        #region variable
        private Dictionary<Type, ObservableCollection<MappingName>> tableColumnMapping = new Dictionary<Type, ObservableCollection<MappingName>>();
        private ObservableCollection<ListMappingName> listTable = new ObservableCollection<ListMappingName>();
        private Type currentType;
        #endregion

        #region contructor
        public AdminManagementCenter()
        {
            InitializeComponent();
            initTableColumn();
            initListTableCombobox();
            addListComboItem();
            var viewModel = this.DataContext as EntityInfoViewModel;
            viewModel.filterChanged = OnFilterChanged;
        }
        #endregion

        #region don't need to care
        private void addListComboItem()
        {
            ListCombo.ItemTemplate = (DataTemplate)Resources["comboboxitemstyle"];
            foreach (var item in listTable)
            {
                ListCombo.Items.Add(item);
            }
        }

        private void addColumnComboItem(Type o)
        {
            var list = tableColumnMapping[o];
            columnCombo.ItemTemplate = (DataTemplate)Resources["comboboxitemstyle"];
            columnCombo.Items.Clear();
            sfGrid.Columns.Clear();
            columnCombo.Items.Add(new MappingName() { Header = Properties.Resources.MappingName_AllColumn, Name = "All Columns" });
            foreach (var mapping in list)
            {
                columnCombo.Items.Add(mapping);
                GridTextColumn column = new GridTextColumn();
                column.HeaderText = mapping.Header;
                column.MappingName = mapping.Name;
                sfGrid.Columns.Add(column);
            }

        }

        private void OnFilterChanged()
        {
            var viewModel = this.DataContext as EntityInfoViewModel;
            if (sfGrid.View != null)
            {
                sfGrid.View.Filter = viewModel.FilerRecords;
                sfGrid.View.RefreshFilter();
            }
        }
        #endregion

        //Hàm để  bắt sự kiện thay đổi danh mục
        private async void ListCombo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            try
            {
                ListMappingName tabletype = (ListMappingName)e.AddedItems[0];
                Type x = tabletype.type;
                currentType = x;
                addColumnComboItem(x);
                object t = new object();

                if (x.Equals(typeof(User)))
                {
                    t = await vc.GetAsync<List<User>>("users");
                }
                else if (x.Equals(typeof(Feature)))
                {
                    t = await vc.GetAsync<List<Feature>>("features");
                }
                else if (x.Equals(typeof(Permission)))
                {
                    t = await vc.GetAsync<List<Permission>>("permissions");
                }
                  
                sfGrid.ItemsSource = t;
            }
            catch (Exception ex)
            {
                var x = ex;
            }
        }

        object currentparameter = new object(); 

        //Hàm để bắt sự kiện select ở bảng
        private void sfGrid_SelectionChanged(object sender, Syncfusion.UI.Xaml.Grid.GridSelectionChangedEventArgs e)
        {
            Btn_Edit.IsEnabled = true;
            Btn_Detail.IsEnabled = true;
            Btn_Delete.IsEnabled = true;
            var a = (GridRowInfo)e.AddedItems[0];
            if (a.RowData.GetType().Equals(typeof(User)))
            {
                User user = (User)a.RowData;
            }
            else if (a.RowData.GetType().Equals(typeof(Feature)))
            {
                Feature feature = (Feature)a.RowData;
            }
            else if (a.RowData.GetType().Equals(typeof(Permission)))
            {
                Permission rfid = (Permission)a.RowData;
            }
              
        }

        //Hàm khởi tạo cột ở bảng
        private void initTableColumn()
        {
            #region xong cai khoi tao cac cot trong bang

             
            ObservableCollection<MappingName> featurecollection = new ObservableCollection<MappingName>();
            featurecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            featurecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Feature_nameen, Name = "nameen" });
            featurecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Feature_namevn, Name = "namevn" });
            featurecollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Feature_description, Name = "description" }); 
            tableColumnMapping.Add(typeof(Feature), featurecollection);

            ObservableCollection<MappingName> usercollection = new ObservableCollection<MappingName>();
            usercollection.Add(new MappingName { Header = Properties.Resources.MappingName_Id, Name = "id" });
            usercollection.Add(new MappingName { Header = Properties.Resources.MappingName_User_username, Name = "username" });
            usercollection.Add(new MappingName() { Header = Properties.Resources.MappingName_User_usergroupId, Name = "usergroup_id" }); 
            tableColumnMapping.Add(typeof(User), usercollection);

     
            ObservableCollection<MappingName> permissioncollection = new ObservableCollection<MappingName>();
            permissioncollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Id, Name = "id" });
            permissioncollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Permission_userGroupID, Name = "usergroup_id" });
            permissioncollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Permission_featureID, Name = "feature_id" });
            permissioncollection.Add(new MappingName() { Header = Properties.Resources.MappingName_Permission_desc, Name = "description" }); 
            tableColumnMapping.Add(typeof(Permission), permissioncollection);

            #endregion xong
            //emp, ser, rf, boki, depar, guest, rôm, billing

        }

        //Thêm các danh mục vào combobox
        private void initListTableCombobox()
        {
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Admin_Feature, type = typeof(Feature) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Admin_Permission, type = typeof(Permission) });
            listTable.Add(new ListMappingName() { Header = Properties.Resources.ListTable_Admin_User, type = typeof(User) });  
        }

        private async void Btn_Delete_Click(object sender, RoutedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);

            try
            {
                if (currentType.Equals(typeof(User)))
                {
                    User a = (User)sfGrid.SelectedItem;
                    await vc.DeleteAsync<User>(a.id);

                }
                else if (currentType.Equals(typeof(Permission)))
                {
                    Permission a = (Permission)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Permission>(a.id);

                }
                else if (currentType.Equals(typeof(Feature)))
                {
                    Feature a = (Feature)sfGrid.SelectedItem;
                    await vc.DeleteAsync<Feature>(a.id);
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show("Deo biet vi sao loi\n" + ex);
            }

            MessageBox.Show("Delete successfully !!!");


        }

        private void Btn_Edit_Click(object sender, RoutedEventArgs e)
        {

            Session.Parameters["isEdit"] = true;
            if (currentType.Equals(typeof(User)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                Session.Parameters["user"] = sfGrid.SelectedItem;
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new UserNew());
            }
            if (currentType.Equals(typeof(Feature)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                Session.Parameters["feature"] = sfGrid.SelectedItem;
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new FeatureCreateNew());
            }
            if (currentType.Equals(typeof(Permission)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry(); 
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new SetPermition());
            }
        }

        private void Btn_Add_New_Click(object sender, RoutedEventArgs e)
        {
            Session.Parameters["isEdit"] = false;
            if (currentType.Equals(typeof(User)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry(); 
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new UserNew());
            }
            if (currentType.Equals(typeof(Feature)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new FeatureCreateNew());
            }
            if (currentType.Equals(typeof(Permission)))
            {
                if (((MainWindow)App.Current.MainWindow).frame.CanGoBack)
                    ((MainWindow)App.Current.MainWindow).frame.NavigationService.RemoveBackEntry();
                ((MainWindow)App.Current.MainWindow).frame.Navigate(new SetPermition());
            }
        }

        private void Btn_Detail_Click(object sender, RoutedEventArgs e)
        {

        }
    }

    #region don't need to care
    /* 
    public class MappingName
    {
        public string Header { get; set; }
        public string Name { get; set; }
    }

   public class ListMappingName
    {
        public string Header { get; set; }
        public Type type { get; set; }
    }
    */
    #endregion
}
