﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for ListRoomInfo.xaml
    /// </summary>
    public partial class ListRoomInfo : Page
    {
        public ListRoomInfo()
        {
            try

            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                // Log error (including InnerExceptions!)
                // Handle exception
                System.Console.WriteLine(ex.ToString());
            }
            this.Loaded += ListRoomInfo_Loaded;

        }

        private async void ListRoomInfo_Loaded(object sender, RoutedEventArgs e)
        {
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            try
            {
                var listem = await vc.GetAsync<List<Room>>("rooms");
                sfGrid_ListRoomInfo.ItemsSource = listem;
            }
            catch { vc.cancelPendingRequests(); }

            var viewModel = this.DataContext as EntityInfoViewModel;
            viewModel.filterChanged = OnFilterChanged;


        }

        private void OnFilterChanged()
        {
            var viewModel = this.DataContext as EntityInfoViewModel;
            if (sfGrid_ListRoomInfo.View != null)
            {
                sfGrid_ListRoomInfo.View.Filter = viewModel.FilerRecords;
                sfGrid_ListRoomInfo.View.RefreshFilter();
            }
        }

        private void sfGrid_ListRoomInfo_SelectionChanged(object sender, Syncfusion.UI.Xaml.Grid.GridSelectionChangedEventArgs e)
        {
            var a = sfGrid_ListRoomInfo;
        }

        private void Room_btn_Add_Click(object sender, RoutedEventArgs e)
        {
            // go to Create new RoomNew
            this.NavigationService.Navigate(new RoomNew());
        }

        private void Room_btn_Order_Click(object sender, RoutedEventArgs e)
        {
            // go to Create new RoomNew
            this.NavigationService.Navigate(new RoomNew());
        }
    }
}
