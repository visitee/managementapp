﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF.Pages
{
    /// <summary>
    /// Interaction logic for CheckInLogReport.xaml
    /// </summary>
    public partial class CheckInLogReport : Page
    {
        public CheckInLogReport()
        {
            InitializeComponent();
            System.Globalization.CultureInfo current = new System.Globalization.CultureInfo(Properties.Resources.CultureInfo);
            fromdate.CultureInfo = current;
            todate.CultureInfo = current;
            this.Loaded += CheckInLogReport_Loaded;
            fromdate.DateTimeChanged += Fromdate_DateTimeChanged;
            todate.DateTimeChanged += Todate_DateTimeChanged;
            
        }

        private ObservableCollection<Model> scatterseriesmodel;
        private ObservableCollection<Model> hiloseriesmodel;
        private Dictionary<int, Room> listroomdic = new Dictionary<int, Room>();
        private ObservableCollection<TextBlock> labels = new ObservableCollection<TextBlock>(); 
        private void Todate_DateTimeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            fromdate.MaxDateTime = todate.DateTime.Value.AddDays(-1);
            setChart();
        }

        private void Fromdate_DateTimeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            todate.MinDateTime = fromdate.DateTime.Value.AddDays(1);
            setChart();
        }

        private async void CheckInLogReport_Loaded(object sender, RoutedEventArgs e)
        {
            scatterseriesmodel = new ObservableCollection<Model>();
            hiloseriesmodel = new ObservableCollection<Model>();
            VisiteeClient vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
            List<Room> listroom = await vc.GetAsync<List<Room>>("rooms");
            foreach(var room in listroom)
            {
                listroomdic.Add(room.id, room);
                var a = await vc.GetSqlAsync<List<CheckInOut>>("select * from checkinouts where room_id=" + room.id);
                var b = await vc.GetSqlAsync<List<Booking>>("select * from bookings where room_id=" + room.id);
                foreach(var checkinout in a)
                {
                    scatterseriesmodel.Add(new Model() {XAxis=room.id, YAxis = checkinout.created_at.ToOADate() });
                }

                foreach(var booking in b)
                {
                    hiloseriesmodel.Add(new Model() { CheckIn = booking.created_at.ToOADate(), CheckOut = booking.updated_at.ToOADate(),XAxis=room.id,tex=room.name });
                }
            }
            
            Chart.PrimaryAxis.VisibleLabels.Clear();
            
            Thread thread = new Thread(setitemsource);
            thread.Start();

            for(int i = 0; i < labels.Count; i++)
            {
                labels[i].Text = listroom[i].name;
            }
            fromdate.MaxDateTime = todate.DateTime.Value.AddDays(-1);
        }

        private void setChart()
        {
            try { 
                axis2.Minimum = fromdate.DateTime.Value;
                axis2.Maximum = todate.DateTime.Value;
                axis2.MaximumLabels = (todate.DateTime.Value.Subtract(fromdate.DateTime.Value).Days)*4;
            }
            catch{ }
        }

        private void setitemsource()
        {
            this.Dispatcher.Invoke((Action)(() =>
            {
                scatterseries.ItemsSource = scatterseriesmodel;
                hiloseries.ItemsSource = hiloseriesmodel;
            }));   
        }

        private Dictionary<int, List<CheckInOut>> RoomCheckInOutLogDictionary;
        private void series_MouseMove(object sender, MouseEventArgs e)
        {
            var y =(Line)e.OriginalSource;
            //y.Stroke = new SolidColorBrush(Colors.Red);
            Syncfusion.UI.Xaml.Charts.HiLoSegment hs = (Syncfusion.UI.Xaml.Charts.HiLoSegment)y.Tag;
            var a = hs.Low;
            //hs.Item = "hihi";
        }

        private void TextBlock_MouseMove(object sender, MouseEventArgs e)
        {
           // var a = (TextBlock)sender;
           // int x;
           // if (int.TryParse(a.Text, out x))
            //    a.Text = listroomdic[x].name;
        }

        private void TextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            labels.Add((TextBlock)sender);
        }
    }

    public class Model
    {
        public double CheckIn { get; set; }
        public double CheckOut { get; set; }
        public DateTime CheckInDateTime
        {
            get
            {
                return DateTime.FromOADate(CheckIn);
            }
        }

        public DateTime CheckOutDateTime
        {
            get
            {
                return DateTime.FromOADate(CheckOut);
            }
        }
        public double XAxis { get; set; }
        public double YAxis { get; set; }
        public DateTime YAxisDateTime
        {
            get
            {
                return DateTime.FromOADate(YAxis);
            }
        }

        public string LabelContent { get; set; }
        public string tex { get; set; }
    }

}
