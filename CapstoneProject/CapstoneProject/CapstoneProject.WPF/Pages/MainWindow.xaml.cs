﻿using CapstoneProject.Classes;
using CapstoneProject.Model;
using CapstoneProject.WPF.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CapstoneProject.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region variable

        private const string color1 = "#FFFFFFFF";
        private const string color2 = "Yellow";
        public Dictionary<string, List<Room>> collection;
        public List<Floor> floors;
        private List<Room> listroom;
        private List<Room> listusingroom;
        private List<CheckInOut> checkinout;

        private bool iscollapse = true;
        private bool firstrun = true;
        private bool candotask = true;
        private bool isdisconnect = false;
        public bool isMenuCollapse() { return iscollapse; }
        private VisiteeClient vc;
        private List<Feature> features;
        private Thread mainThread;
        private Thread thread;
        #endregion

        public MainWindow()
        {
            InitializeComponent();
            //this.Content = new PlatformVisualsPage();
            this.Loaded += MainWindow_Loaded;
            Session.OnAuthenticationFinished += OnAuthenticationFinished;
            Session.OnSessionStateChanged += OnSessionStateChanged;
            mainThread = new Thread(_animatemenu);
            thread = new Thread(_datalistener);
        }

        private void OnSessionStateChanged(LoginStatus status)
        {
            if (status.Equals(LoginStatus.LoggedOut))
            {
                frame.Navigate(new Login());
                leftmenu.clearMenu();
                topbar.collapsedTopBar();
                if (thread != null)
                {
                    //thread.Abort();
                    candotask = false;
                }
            }
        }

        #region LoadData
        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using (StreamReader sr = new StreamReader("IPconfig.txt"))
                {
                    String line = await sr.ReadToEndAsync();
                    CapstoneProject.App.Url = line.Trim();
                }
            }
            catch (Exception ex)
            {
            }
            if (String.IsNullOrEmpty(Session.ActiveSession.CurrentAccessTokenData.AccessToken))
            {
                if (frame.CanGoBack)
                    frame.NavigationService.RemoveBackEntry();
                frame.Navigate(new Login());
            }
            else
            {
                loadData();
                topbar.setTopBar();
            }
        }

        private void OnAuthenticationFinished(AccessTokenData session)
        {
            if (session.AccessToken != null)
            {
                loadData();
                topbar.setTopBar();
            }
        }

        private async void loadData()
        {
            collection = new Dictionary<string, List<Room>>();
            floors = new List<Floor>();
            listroom = new List<Room>();
            checkinout = new List<CheckInOut>();
            features = new List<Feature>();
            listusingroom = new List<Room>();

            try
            {
                vc = new VisiteeClient(Session.ActiveSession.CurrentAccessTokenData.AccessToken, VisiteeClient.VisiteeClientType.Token);
                floors = await vc.GetAsync<List<Floor>>("floors");

                features = await vc.GetAsync<List<Feature>>("features");
                foreach (var feature in features)
                {
                    feature.image = vc.getFeatureImage(feature.id);
                }
                leftmenu.setMenu(features);
                if (frame.CanGoBack)
                    frame.NavigationService.RemoveBackEntry();
                frame.Navigate(new ListRoom());
            }
            catch { vc.cancelPendingRequests();
                //frame.Navigate(new ErrorNetworkDisconnect());
            }
            if (firstrun) {  
                thread.Start();
                firstrun = false;
            }
            else {
                candotask = true;
                thread = new Thread(_datalistener);
                thread.Start();
            }
        }

        private async void _datalistener()
        {
            while (candotask)
            {
                
                try
                {
                    listroom = await vc.GetAsync<List<Room>>("rooms");
                    listusingroom = await vc.GetSqlAsync<List<Room>>("select * from rooms where status = 2 order  by updated_at desc");
                    checkinout = await vc.GetAsync<List<CheckInOut>>("checkinouts");
                    isdisconnect = false;
                }
                catch { vc.cancelPendingRequests();
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        //candotask = false;
                        isdisconnect = true;
                        leftmenu.clearMenu();
                        topbar.collapsedTopBar();
                        frame.Navigate(new ErrorNetworkDisconnect());
                    }));
                }
                if (!isdisconnect)
                {
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                        candotask = true;
                        if (Session.ActiveSession.CurrentAccessTokenData!=null&&!(leftmenu.chilcount > 0)) {
                            //loadData();
                            
                            leftmenu.setMenu(features);
                                topbar.setTopBar();
                        }
                    }));
                }

                foreach (var floor in floors)
                {
                    List<Room> a = new List<Room>();


                    foreach (Room r in listroom)
                    {
                        r.image = Properties.Resources.Svg_Room;
                        if (r.status == 1)
                            r.color = color1;
                        else {
                            r.color = color2;
                            for (int i = 0; i < checkinout.Count; i++)
                            /*{
                                if (checkinout[i].room_id == r.id)
                                    r.firstcheckin = checkinout[i].created_at;
                            }*/
                            //for (int i = checkinout.Count - 1; i >= 0; i--)
                            {
                                if (checkinout[i].room_id == r.id)
                                    r.lastcheckin = checkinout[i].created_at;
                            }
                        }
                        if (r.floor_id == floor.id)
                            a.Add(r);
                    }

                    collection[""+floor.id] = a;
                    this.Dispatcher.Invoke((Action)(() =>
                    {
                    
                    if (frame.Content!=null&&frame.Content.GetType().Equals(typeof(ListRoom)))
                    {
                        ((ListRoom)frame.Content).setBinding(collection,listroom.Count,listusingroom.Count);
                    }
                    }));

                }
                this.Dispatcher.Invoke((Action)(() =>
                {
                    notification.setBinding(listusingroom);
                }));

                Thread.Sleep(3000);
            }
        }

        #endregion

        #region animation
        public void animatemenu()
        {
            if (iscollapse)
            {
                mainThread.Abort();
                mainThread = new Thread(_animatemenu);
                mainThread.Start();

                iscollapse = false;
            }
            else {
                mainThread.Abort();
                mainThread = new Thread(_animatemenu2);
                mainThread.Start();
                //columndefinition.Width = new GridLength(40);
                iscollapse = true;

            }
        }

        private void _animatemenu()
        {
            
                while (true)
                {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    columndefinition.Width = new GridLength(columndefinition.Width.Value + 2.5);
                }));
                //Thread.Sleep(1);
                this.Dispatcher.Invoke((Action)(() =>
                {
                    if (columndefinition.Width.Value >= 200)
                        mainThread.Abort();
                }));
                
            }
            
            
        }

        private void _animatemenu2()
        {

            while (true)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    columndefinition.Width = new GridLength(columndefinition.Width.Value - 2.5);
                }));
                //Thread.Sleep(1);
                this.Dispatcher.Invoke((Action)(() =>
                {
                    if (columndefinition.Width.Value <= 40)
                        mainThread.Abort();
                }));

            }


        }
        
        #endregion

        public void setNotifyVisibility(Visibility visibility)
        {
                notification.Visibility = visibility;
        }
    }
}
