﻿using CapstoneProject.Model;
using Syncfusion.Windows.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace CapstoneProject.WPF
{
        class EntityInfoViewModel : INotifyPropertyChanged
        {
            public EntityInfoViewModel()
            {

                //EmployeeDetails = new EmployeeInfoRespository().GetEmployeesDetails(2000);
            }


            internal FilterChanged filterChanged;
            private ICommand textchanged;

            public ICommand TextChanged
            {
                get { return new DelegateCommand<object>(TextChangeEvent, args => true); }
                set { textchanged = value; OnPropertyChanged("TextChanged"); }
            }

            public ICommand ComboChanged
            {
                get { return new DelegateCommand<object>(ComboxChangedEvent, args => { return true; }); }
            }

            public ICommand FilterComboChanged
            {
                get { return new DelegateCommand<object>(FilterComboxChangedEvent, args => { return true; }); }
            }

            private void FilterComboxChangedEvent(object pram)
            {
                if (pram != null)
                {
                    this.FilterOption = (pram as MappingName).Name;
                if(filterChanged!=null)
                    filterChanged();
                }
            }

            private void ComboxChangedEvent(object pram)
            {
                if (pram != null)
                {
                    this.FilterCondition = (pram as ComboBoxItem).Tag.ToString();
                    filterChanged();
                }
            }

            private void TextChangeEvent(object pram)
            {
                if (pram != null)
                {
                    this.FilterText = pram.ToString();
                    filterChanged();
                }
            }

            private bool MakeStringFilter<T>(T o, string option, string condition)
            {
                var value = o.GetType().GetProperty(option);
                var exactValue = value.GetValue(o, null);
                if (exactValue == null)
                    exactValue = "";
                exactValue = exactValue.ToString().ToLower();
                string text = FilterText.ToLower();
                var methods = typeof(string).GetMethods();
                if (methods.Count() != 0)
                {
                    var methodInfo = methods.FirstOrDefault(method => method.Name == condition);
                    bool result1 = (bool)methodInfo.Invoke(exactValue, new object[] { text });
                    return result1;
                }
                else
                    return false;
            }

            private bool MakeNumericFilter<T>(T o, string option, string condition)
            {
                var value = o.GetType().GetProperty(option);
                var exactValue = value.GetValue(o, null);
                double res;
                bool checkNumeric = double.TryParse(exactValue.ToString(), out res);
                checkNumeric = false;
                if (checkNumeric)
                {
                    switch (condition)
                    {
                        case "Equals":
                            if (Convert.ToDouble(exactValue) == (Convert.ToDouble(FilterText)))
                                return true;
                            break;
                        case "GreaterThan":
                            if (Convert.ToDouble(exactValue) > Convert.ToDouble(FilterText))
                                return true;
                            break;
                        case "LessThan":
                            if (Convert.ToDouble(exactValue) < Convert.ToDouble(FilterText))
                                return true;
                            break;
                        case "NotEquals":
                            if (Convert.ToDouble(FilterText) != Convert.ToDouble(exactValue))
                                return true;
                            break;
                    }
                }
                return false;
            }

            public bool FilerRecords<T>(T o)
            {

            double res;
                bool checkNumeric = double.TryParse(FilterText, out res);
                checkNumeric = false;
                var item = o;
                var type = o.GetType();
            
                if (item != null && FilterText.Equals(""))
                {
                    return true;
                }
                else
                {
                    if (item != null)
                    {
                        if (checkNumeric && !FilterOption.Equals("All Columns"))
                        {
                            if (FilterCondition == null || FilterCondition.Equals("Contains") || FilterCondition.Equals("StartsWith") || FilterCondition.Equals("EndsWith"))
                                FilterCondition = "Equals";
                            bool result = MakeNumericFilter(item, FilterOption, FilterCondition);
                            return result;
                        }
                        else if (FilterOption.Equals("All Columns"))
                        {

                            Type t = o.GetType();
                            var properties = t.GetProperties();
                            foreach (var property in properties)
                            {
                                if (MakeStringFilter(o,property.Name,"Contains"))
                                    return true;
                            }
                            return false;
                        }
                        else
                        {
                            if (FilterCondition == null || FilterCondition.Equals("Equals") || FilterCondition.Equals("LessThan") || FilterCondition.Equals("GreaterThan") || FilterCondition.Equals("NotEquals"))
                                FilterCondition = "Contains";
                            bool result = MakeStringFilter(item, FilterOption, FilterCondition);
                            return result;
                        }
                    }
                }
                return false;
            }

            private string filterOption = "All Columns";

            public string FilterOption
            {
                get { return filterOption; }
                set
                {
                    filterOption = value;
                    OnPropertyChanged("FilterOption");
                }
            }

            private string filterText = string.Empty;

            public string FilterText
            {
                get { return filterText; }
                set
                {
                    filterText = value;
                    OnPropertyChanged("FilterText");
                }
            }

            private string filterCondition;

            public string FilterCondition
            {
                get { return filterCondition; }
                set
                {
                    filterCondition = value;
                    OnPropertyChanged("FilterCondition");
                }
            }



            public event PropertyChangedEventHandler PropertyChanged;

            public void OnPropertyChanged(string propertyName)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    var e = new PropertyChangedEventArgs(propertyName);
                    handler(this, e);
                }
            }
        }
        internal delegate void FilterChanged();
}
